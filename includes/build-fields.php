<?php if( function_exists('register_field_group') ):


        register_field_group(array (
            'key' => 'group_556ed19a924c8',
            'title' => 'Vacancy Post',
            'fields' => array (
                array (
                    'key' => 'field_556ed1a98189f',
                    'label' => 'Location',
                    'name' => 'location',
                    'prefix' => '',
                    'type' => 'checkbox',
                    'instructions' => '',
                    'required' => 1,
                    'conditional_logic' => 0,
                    'wrapper' => array (
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                    'choices' => array (
                        'Wokingham' => 'Wokingham',
                        'Devon' => 'Devon',
                        'Colombo' => 'Colombo',
                    ),
                    'default_value' => array (
                        '' => '',
                    ),
                    'layout' => 'vertical',
                ),
                array (
                    'key' => 'field_556ed4e7818a0',
                    'label' => 'Department',
                    'name' => 'department',
                    'prefix' => '',
                    'type' => 'radio',
                    'instructions' => '',
                    'required' => 1,
                    'conditional_logic' => 0,
                    'wrapper' => array (
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                    'choices' => array (
                        'Technology' => 'Technology',
                        'Client Services' => 'Client Services',
                        'Creative' => 'Creative',
                        'Operational' => 'Operational',
                        'Learning & Development' => 'Learning & Development',
                    ),
                    'other_choice' => 0,
                    'save_other_choice' => 0,
                    'default_value' => '',
                    'layout' => 'vertical',
                ),
            ),
            'location' => array (
                array (
                    array (
                        'param' => 'post_type',
                        'operator' => '==',
                        'value' => 'vacancies',
                    ),
                ),
            ),
            'menu_order' => 0,
            'position' => 'acf_after_title',
            'style' => 'default',
            'label_placement' => 'top',
            'instruction_placement' => 'label',
            'hide_on_screen' => '',
        ));

endif; ?>