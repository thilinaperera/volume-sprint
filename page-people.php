<?php
/**
 * Template Name: People
 *
 *
 * @link
 * @since 1.0
 *
 * @package WordPress
 * @subpackage Volume People
 * @author Kashif
 */
get_header(); ?>
<style>
    /*media queries*/

    @media only screen and (min-width: 700px) and (max-width: 1100px) {
        /*.people-main .slide-container .slider-content.sun-seek .avatar,
        .people-main .slide-container .slider-content.party .avatar,
        .people-main .slide-container .slider-content.culinary .avatar,
        .people-main .slide-container .slider-content.gaming .avatar,
        .people-main .slide-container .slider-content.sports .avatar,
        .people-main .slide-container .slider-content.trendsetter .avatar,
        .people-main .slide-container .slider-content.bark .avatar { margin-bottom: 35px; }*/
    }

    @media only screen and (min-width: 1100px) and  (max-width: 1700px) {

        /*.people-main .slide-container .slider-content.sun-seek .avatar,
        .people-main .slide-container .slider-content.party .avatar,
        .people-main .slide-container .slider-content.culinary .avatar,
        .people-main .slide-container .slider-content.gaming .avatar,
        .people-main .slide-container .slider-content.sports .avatar,
        .people-main .slide-container .slider-content.trendsetter .avatar,
        .people-main .slide-container .slider-content.bark .avatar { margin-bottom: 28px; }*/
    }

    @media only screen and (max-width : 940px) {

    }
</style>

    <div class="people-main site-body">
        <div class="body-bg"></div>
        <div class="header-sub full-width" id="1">
            <div class="wrap">
                <div class="copy-area">
                    <h1>Fun-loving<span> People</span></h1>
                    <p>Our unique, high-energy culture is shaped by incredibly smart and passionate individuals from all walks of life.</p>
                </div>
            </div>
        </div>

<div class="parallax-holder">
 <div class="parallax-content">
 <div class="waypoint-wrapper">
   <div class="content-main full-width" id="2">
       <div class="wrap">
            <div class="content-holder">
                <h2>A Volume<span> Person</span></h2>
                <hr/>
                <p>Volume People are unique... yet united by the same passion, determination and love of learning that has crafted our culture.</p>
                <p>Healthily self-critical, tenacious, loyal and honest, ‘the Volume Person’ is wonderful to work with. We hope you’ll come and see for yourself.</p>

                <h2 class="last-sub">A Volume<span> Person's</span> View</h2>
                <hr/>
                <p>See what our People have to say about life at Volume.</p>
            </div>
           <div class="content-holder">

               <div class="video-holder">
                   <iframe width="100%" height="100%" src="https://www.youtube.com/embed/on5kky1Z7ec?rel=0"
                           frameborder="0" allowfullscreen></iframe>
               </div>
           </div>
       </div>
       <div class="clear"></div>
   </div>
   <div class="quote-main full-width" id="3">
       <div class="wrap">
            <div class="quote-holder">
                <div class="quote-holder-wrapper">
                    <!--<div class="open-quote"></div> -->
                    <blockquote class="open-block">
                        <p class="blockquote">Our people work hard&#8230; and play hard, too. Because when we&rsquo;re having fun, we get more done&#33;</p>
                    </blockquote >
                </div>
            </div>
       </div>
   </div>
   <div class="slide-container full-width" id="4">
            <div class="slide-show">
                <div class="slider-content slide1 music">
                    <div class="wrap">
                        <div class="avatar"></div>
                        <h2>Music<span> Mogul</span></h2>
                        <hr/>
                        <p>With a Sonos speaker system set up across our entire office, there’s never a quiet moment here at Volume! (Psst… we work smart, which means we can all do our jobs remotely from any location worldwide. That includes our silent areas, in case you’re craving some extra concentration!)</p>
                    </div>
                    <div class="image-gallery">
                        <img src="<?php bloginfo('stylesheet_directory'); ?>/images/slide-music.jpg"  alt="">

                    </div>
                </div>
                <div class="slider-content slide1 sun-seek">
                    <div class="wrap">
                        <div class="avatar"></div>
                        <h2>Sun<span> Seeker</span></h2>
                        <hr/>
                        <p>Our kitchen is cool… but our garden is hotter! We love grabbing a picnic blanket and heading outside for lunch over a game of giant Jenga.
                        </p>
                    </div>
                    <div class="image-gallery">
                        <img src="<?php bloginfo('stylesheet_directory'); ?>/images/slide-sunseek.jpg"  alt="">

                    </div>
                </div>
                <div class="slider-content slide1 party">
                    <div class="wrap">
                    <div class="avatar"></div>
                    <h2>Party<span> Animal</span></h2>
                    <hr/>
                    <p>What would Christmas be without an outrageous office party? And what would summer be without an afternoon of outdoor antics? And what would weekends be without dinners, drinks and dancing? Okay… we admit it. Any excuse for a celebration, and we’re there.
                    </p>
                    </div>
                    <div class="image-gallery">
                        <img src="<?php bloginfo('stylesheet_directory'); ?>/images/slide-party.jpg"  alt="">

                    </div>
                </div>
                <div class="slider-content slide3 culinary ">
                    <div class="wrap">
                        <div class="avatar"></div>
                        <h2>Culinary<span> connoisseur</span></h2>
                        <hr/>
                        <p>There’s no doubt about it, Volume is full of foodies… and, of course, we cater accordingly! With bake-offs, pancake making, international food feasts and Fish & Chip Fridays, it’s impossible to go hungry in our offices. Thank goodness for our discounted gym memberships!
                        </p>
                    </div>
                    <div class="image-gallery">
                        <img src="<?php bloginfo('stylesheet_directory'); ?>/images/slide-culinary.jpg"  alt="">
                    </div>
                </div>
                <div class="slider-content slide3 gaming">
                    <div class="wrap">
                        <div class="avatar"></div>
                        <h2>Gaming<span> Guru</span></h2>
                        <hr/>
                        <p>Always chasing that ever-elusive high score? Put in some hours at the office on our Xbox Kinect… because you’ve never really experienced gaming until you’ve played on a giant projector screen!</p>
                    </div>
                    <div class="image-gallery">
                        <img src="<?php bloginfo('stylesheet_directory'); ?>/images/slide-gameguru.jpg"  alt="">
                    </div>
                </div>
                <div class="slider-content slide1 sports">
                    <div class="wrap">
                        <div class="avatar"></div>
                        <h2>Sports<span> Superstar</span></h2>
                        <hr/>
                        <p>Engage your endorphins at Volume! From sports days and sponsored runs, football and archery, there are plenty of ways to stay active here.</p>
                    </div>
                    <div class="image-gallery">
                        <img src="<?php bloginfo('stylesheet_directory'); ?>/images/slide-sports.jpg"  alt="">
                    </div>
                </div>
                <div class="slider-content slide3 trendsetter">
                    <div class="wrap">
                        <div class="avatar"></div>
                        <h2>Tech<span> Trendsetter</span></h2>
                        <hr/>
                        <p>Do you own more gizmos than Inspector Gadget? Or is James Bond’s ‘Q’ more like you? Either way, you’ll fit in famously at Volume – the technology hub of the Thames Valley – as you enjoy dabbling with the latest devices!</p>
                    </div>
                    <div class="image-gallery">
                        <img src="<?php bloginfo('stylesheet_directory'); ?>/images/slide-trendsetter.jpg"  alt="">
                    </div>
                </div>
                <div class="slider-content slide3 bark">
                    <div class="wrap">
                        <div class="avatar"></div>
                        <h2>Barking<span> Mad</span></h2>
                        <hr/>
                        <p>Is your best friend furry and four-legged? Our annual Bring Your Dog to Work day is sure to get their tail wagging!</p>
                    </div>
                    <div class="image-gallery">
                        <img src="<?php bloginfo('stylesheet_directory'); ?>/images/slide-dogs.jpg"  alt="">
                    </div>
                </div>

            <a id=prev>Prev</a>
            <a id=next>next</a>
        </div>

 </div>
</div>
     <?php get_footer(); ?>
</div><!--end paralax content-->
 </div><!--end paralax holder-->
        </div>

