<?php
/**
 * The template for displaying the header
 *
 *
 * @link
 * @since 1.0
 *
 * @package WordPress
 * @subpackage Volume People
 * @author Kashif
 */
?>

<!doctype html>
<html class="fsvs" lang="en" xmlns:fb="http://ogp.me/ns/fb#">
<head>
    <?php wp_head(); ?>
    <script>var $ = jQuery.noConflict();</script>
    <?php
    get_template_part("includes/build","meta"); 
    ?>

</head>
<body class="body-<?=  is_front_page() ? 'landing-page' : strtolower( str_replace(" ", "-", get_the_title()) );  ?>">

<div class="mobile-splash">
    <div class="splash-content">
        <div class="box">
        </div>
        <div class="clear"></div>
        <div class="text">
            <h2>Please rotate your device</h2>
        </div>
    </div>
</div>
<div class="home-header <?=  is_front_page() ? 'landing-page' : strtolower( str_replace(" ", "-", get_the_title()) );  ?>">
    <div class="header-inner">
      <div class="wrap">
    <a href="<?php bloginfo('url') ?>" class="logo">

    </a>
     <?php wp_nav_menu( array( 'theme_location' => 'header-menu') );?>
          <a class="nav-cta">Open</a>
    <a href="<?php bloginfo('url') ?>" class="nav-cta mob-button">Open</a>
      </div>
    </div>
</div>