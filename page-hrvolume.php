<?php
/**
 * Template Name: HR-Volume
 *
 * @link
 * @since 1.0
 *
 * @package WordPress
 * @subpackage Volume People
 * @author Kashif
 */
get_header(); ?>

<?php get_header() ?>

    <link href="<?php echo get_template_directory_uri(); ?>/styles/jquery.dataTables.min.css" rel="stylesheet">

    <script>
        $(document).ready(function() {
            $('#customertable').dataTable();
        });
    </script>


        <div class="hrtable">
            <h2>
                Vacancies Summary
            </h2>

            <table  style="width:100%" border="1" id="customertable">
                <tr>

                    <th>job Title</th>
                    <th>Location</th>
                    <th>Department</th>
                    <th>Edit</th>
                </tr>

    <?php



    $args = array('post_type' => 'vacancies', 'posts_per_page' => -1 );
    $the_query = new WP_Query( $args );


    ?>


    <?php if ( have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post(); ?>


                <tr>
                    <td><?php the_title(); ?> </td>
                    <td><?php the_field('location'); ?> </td>
                    <td><?php the_field('department'); ?></td>
                    <td><?php edit_post_link('Edit');?> </td>
                </tr>
    <?php endwhile; endif; wp_reset_Query(); ?>

            </table>


        </div>


<?php get_footer(); ?>

