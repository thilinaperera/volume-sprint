<?php
/**
 * Customs RSS template with related posts.
 *
 * Place this file in your theme's directory.
 *
 * @package sometheme
 * @subpackage theme
 */

add_filter( 'w3tc_can_print_comment', function( $w3tc_setting ) { return false; }, 10, 1 );

/**
 * Get related posts.
 */
function my_rss_related() {

    global $post;

    // Setup post data
    $pid     = $post->ID;
    $tags    = wp_get_post_tags( $pid );
    $tag_ids = array();

    // Loop through post tags
    foreach ( $tags as $individual_tag ) {
        $tag_ids[] = $individual_tag->term_id;
    }

    // Execute WP_Query
    $related_by_tag = new WP_Query( array(
        'tag__in'          => $tag_ids,
        'post__not_in'     => array( $pid ),
        'posts_per_page'   => 3,
    ) );

    // Loop through posts and build HTML
    if ( $related_by_tag->have_posts() ) :

        echo 'Related:<br />';

        while ( $related_by_tag->have_posts() ) : $related_by_tag->the_post();
            echo '<a href="' . get_permalink() . '">' . get_the_title() . '</a><br />';
        endwhile;

    else :
        echo '';
    endif;

    wp_reset_postdata();
}


/**
 * Feed defaults.
 */
header( 'Content-Type: ' . feed_content_type( 'rss-http' ) . '; charset=' . get_option( 'blog_charset' ), true );
$frequency  = 1;        // Default '1'. The frequency of RSS updates within the update period.
$duration   = 'hourly'; // Default 'hourly'. Accepts 'hourly', 'daily', 'weekly', 'monthly', 'yearly'.
$postlink   = '<br /><a href="' . get_permalink() . '">See the rest of the story at '.get_permalink().'</a><br /><br />';
$postimages = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), 'large' );

// Check for images
if ( $postimages ) {

    // Get featured image
    $postimage = $postimages[0];

} else {

    // Fallback to a default
    $postimage = IMAGES . '/rss.jpg';
}


/**
 * Start RSS feed.
 */
echo '<?xml version="1.0" encoding="' . get_option( 'blog_charset' ) . '"?' . '>'; ?>

<rss version="2.0"
     xmlns:content="http://purl.org/rss/1.0/modules/content/"
     xmlns:wfw="http://wellformedweb.org/CommentAPI/"
     xmlns:dc="http://purl.org/dc/elements/1.1/"
     xmlns:atom="http://www.w3.org/2005/Atom"
     xmlns:sy="http://purl.org/rss/1.0/modules/syndication/"
     xmlns:slash="http://purl.org/rss/1.0/modules/slash/"
    <?php do_action( 'rss2_ns' ); ?>
>
    <channel>
        <title><?php bloginfo_rss( 'name' ); wp_title_rss(); ?></title>
        <link><?php bloginfo_rss( 'url' ) ?></link>
        <description><?php bloginfo_rss( 'description' ) ?></description>
        <lastBuildDate><?php echo mysql2date( 'D, d M Y H:i:s +0000', get_lastpostmodified( 'GMT' ), false ); ?></lastBuildDate>
        <language><?php bloginfo_rss( 'language' ); ?></language>
        <sy:updatePeriod><?php echo apply_filters( 'rss_update_period', $duration ); ?></sy:updatePeriod>
        <sy:updateFrequency><?php echo apply_filters( 'rss_update_frequency', $frequency ); ?></sy:updateFrequency>
        <atom:link rel="hub" href="http://pubsubhubbub.appspot.com"/>
        <atom:link href="<?php self_link(); ?>" rel="self" type="application/rss+xml" />
        <image>
            <url><?= IMAGES . '/rss.jpg'; ?></url>
            <title>
                <?php bloginfo_rss( 'description' ) ?>
            </title>
            <link><?php bloginfo_rss( 'url' ) ?></link>
        </image>

        <?php do_action( 'rss2_head' ); ?>

        <?php
        $args = array(
            'post_type' => 'jobman_job',
            'posts_per_page' => -1,
            'date_query'    => array(
                'column'  => 'post_date',
                'after'   => '- 90 days'
            )
        );
        $the_query = new WP_Query($args);
        ?>
        <?php while( $the_query->have_posts() ) : $the_query->the_post(); ?>

            <item>
                <title><?php the_title_rss(); ?></title>
                <link><?php the_permalink_rss(); ?></link>
                <guid isPermaLink="false"><?php the_guid(); ?></guid>
                <author><?php the_author(); ?> &lt;<?= get_the_author_meta( 'user_email' ); ?>&gt;</author>
                <pubDate><?php echo mysql2date( 'D, d M Y H:i:s +0000', get_post_time( 'Y-m-d H:i:s', true ), false ); ?></pubDate>
                <content:encoded><![CDATA[ <figure><img src="<?= IMAGES . '/rss.jpg'; ?>"><figcaption><?= get_the_title() ?></figcaption></figure><p><?=  get_post_meta(get_the_ID(), 'data5', true)  ?></p> ]]></content:encoded>
            </item>
        <?php endwhile; ?>
    </channel>
</rss>