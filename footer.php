<?php
/**
 * The template for displaying the footer
 *
 *
 * @link
 * @since 1.0
 *
 * @package WordPress
 * @subpackage Volume People
 * @author Kashif
 */
?>


<div class="footer full-width" id="10">
<div class="wrap">
    <ul class="footer-top">
        <li class="tweet content-column">
            <h5>Latest tweet</h5>


<!--            <p> --><?//= display_latest_tweets('andrew_biggart'); ?><!-- </p>-->

            <p><?php

    /* your parameters */
    $jltw_args = array(
        'username'  => 'VolumeLtd',
        'nb_tweets' => 1,
        'avatar'    => false,
        'cache'     => 1600,
        'transition'    => true,
        'delay'     => 10,
        'links'     => false
    );

    /* set variable */

    $list_of_tweets = get_jltw($jltw_args);

    /* more later */
    echo $list_of_tweets;?> </p>
        </li>
        <li class="social mobile-hide content-column">
            <h5>Social</h5>
            <ul>
                <li><a href="https://www.facebook.com/VolumeLtd" target="_blank" class="fb"></a></li>
                <li><a href="https://twitter.com/VolumeLtd" target="_blank" class="twitter"></a></li>
                <li><a href="https://www.youtube.com/user/VolumeGroup" target="_blank" class="you-tube"></a></li>
                <li><a href="https://www.linkedin.com/company/volume" target="_blank" class="linkedin"></a></li>
                <!--<li><a href="#" class="pintrest"></a></li>-->
                <li><a href="https://www.behance.net/VolumeLtd" target="_blank" class="behance"></a></li>
            </ul>
        </li>
        <li class="content-column"><h5>Telephone</h5>
            <p>+44 (0)118 977 5800</p>
            <h5>Address</h5>
            <p class="mobile-hide">Buckhurst Court<br>
                London Road<br>
                Wokingham<br>
                Berkshire<br>
                RG40 1PA</p>
            <p class="only-potrait">Buckhurst Court, London Road <br>Wokingham, Berkshire<br> RG40 1PA</p>
        </li>
        <li class="content-column">
            <h5>email</h5>
            <p><a href="mailto:careers@volume.ai">careers@volume.ai</a></p>
            <h5>Web</h5>
            <p><a href="http://www.volume.ai" target="_blank">www.volume.ai</a><br>
            <a href="http://www.cogcom.ai" target="_blank">www.cogcom.ai</a></p>
        </li>
        <li class="social content-column only-potrait">
            <ul>
                <li><a href="https://www.facebook.com/VolumeLtd" target="_blank" class="fb"></a></li>
                <li><a href="https://twitter.com/VolumeLtd" target="_blank" class="twitter"></a></li>
                <li><a href="https://www.youtube.com/user/VolumeGroup" target="_blank" class="you-tube"></a></li>
                <li><a href="https://www.linkedin.com/company/volume" target="_blank" class="linkedin"></a></li>
                <!--<li><a href="#" class="pintrest"></a></li>-->
                <li><a href="https://www.behance.net/VolumeLtd" target="_blank" class="behance"></a></li>
            </ul>
        </li>
    </ul>
    <div class="clear"></div>
    <div class="footer-bottom">
        <ul class="left">
            <li><p>&copy; Volume Ltd. <?= date('Y') ?> </p></li>
            <li><a href="https://volumeglobal.com/HOW-SERIOUSLY-DO-YOU-TAKE-DATA-PROTECTION" target="_blank">Privacy</a></li>
            <li><a href="https://volumeglobal.com/can-you-tell-me-about-your-terms-and-conditions" target="_blank">Terms & Conditions</a></li>
            <li><a href="https://volumeglobal.com/can-you-tell-me-about-your-cookies" target="_blank">How we use cookies</a></li>
            <li><a href="https://volumeglobal.com/who-are-the-leadership-at-volume" target="_blank">Leadership</a></li>
            <li><a href="<?php echo get_template_directory_uri(); ?>/docs/Corporate-Social-Responsibility-Statement-Volume 2016.pdf" target="_blank">CSR Strategy</a></li>
        </ul>
        <ul class="right">
            <li><a class="be" href="http://www.b.co.uk/Company/Profile/375122/" target="_blank"></a></li>
            <li><a class="ai" href="http://www.acquisition-intl.com/" target="_blank"></a></li>
            <li><a class="magazin" href="http://www.businessawards.co.uk/images-from-tvbma-2015/" target="_blank"></a></li>
            <li><a class="learning" href="http://www.learningandperformanceinstitute.com/" target="_blank"></a></li>
            <li><a class="watson" href="http://www.ibm.com/smarterplanet/us/en/ibmwatson/" target="_blank"></a></li>
        </ul>
    </div>
    <div class="clear"></div>
</div>

</div>
<?php wp_footer(); ?> 
</body>
</html>
