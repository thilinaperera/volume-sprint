<?php
/**
 * ajax application controller
 *
 * Used to upload applications via ajax & create "application" custom posts
 *
 * @link
 * @since 1.0
 *
 * @package WordPress
 * @subpackage Volume People
 * @author Thilina Perera
 */
$parse_uri = explode( 'wp-content', $_SERVER['SCRIPT_FILENAME'] );
require_once( $parse_uri[0] . 'wp-load.php' );


if(isset($_FILES['file'])){
    $dir = dirname( $_SERVER['SCRIPT_FILENAME'] );

    if( ! file_exists( "$dir/wp-admin/includes/file.php" ) )
        $dir = WP_CONTENT_DIR . '/..';
    require_once( "$dir/wp-admin/includes/file.php" );
    require_once( "$dir/wp-admin/includes/image.php" );
    require_once( "$dir/wp-admin/includes/media.php" );

    $temp = explode(".", $_FILES['file']['name'] );
    $allowedExts = array("pdf","doc","docx");
    $extension = end($temp);
    if( in_array($extension, $allowedExts) && $_FILES['file']['size'] < 52428800 ){

        if( is_uploaded_file( $_FILES['file']['tmp_name'] ) ) {
            /**
             * upload file as a wp media without attaching any post " use 0 for this"
             */
            if($_POST['attachments'] === 'true'){
                $data = media_handle_upload( "file", 0, array( 'post_status' => 'private' ) );
                if( is_wp_error( $data ) ) {
                    $result = array(
                        'code' => 500,
                        'file' => 0,
                        'url' => 0,
                    );
                    echo json_encode($result);
                }else{
                    $result = array(
                        'code' => 202,
                        'file' => $data,
                        'url' => wp_get_attachment_url($data),
                        'name' => $_FILES['file']['name'],
                        'full' => $_POST['attachments']
                    );
                    echo json_encode($result);
                }
            }else{
                $result = array(
                    'code' => 406,
                    'file' => 0,
                    'url' => 0,
                );
                echo json_encode($result);
            }

        }
    }
    else{
        echo "{'message : 'file format not allowed'}";
    }

}
if( isset(  $_POST['action'] )){
    $action = $_POST['action'];
    $data = $_POST['data'];
    if($action == 'delete'){
        $return  = wp_delete_attachment( $data['id'], true );
        if($return){
            $result = array(
                'code' => 200,
                'file' => $data['id'],
                'status' => 'deleted'
            );
        }else{
            $result = array(
                'code' => 404,
                'file' => $data['id'],
                'status' => 'not found'
            );
        }
        echo json_encode($result);
    }
    elseif( $action == 'submit' ){

        $job_id = $data['job'];
        $category_slug = $data['slug'];
        $attachment = $data['attachment'];

        $name = $_POST['name'];
        $email = $_POST['email'];
        $cover = $_POST['cover'];


        $options = get_option( 'jobman_options' );

        $page = array(
            'comment_status' => 'closed',
            'ping_status' => 'closed',
            'post_status' => 'private',
            'post_type' => 'jobman_app',
            'post_content' => '',
            'post_title' => __( 'Application', 'jobman' ),
            'post_parent' => $options['main_page']
        );

        $appid = wp_insert_post( $page );
        wp_set_object_terms( $appid, $category_slug, 'jobman_category', true );

        add_post_meta( $appid, 'job', $job_id, false );
        add_post_meta( $attachment, '_jobman_attachment', 1, true );
        add_post_meta( $attachment, '_jobman_attachment_upload', 1, true );
        add_post_meta( $appid, "data18", $attachment, true );
        add_post_meta( $appid, "data2", $name, true );
        add_post_meta( $appid, "data4", $email, true );
        mailFunc($attachment,$job_id,$name,$email,$cover);
        $result = array(
            'code' => 201,
            'file' => $attachment,
            'post' => $appid,
            'applicant' => array(
                'name' => $name,
                'email' => $email
            ),
            'status' => 'new post created'
        );
        echo json_encode($result);
    }
};

/**
 * wp mail sender with custom email template
 *
 * @param $fileID number attachment id
 * @param $id number job post id
 * @param $name string  applicant name
 * @param $email string applicant email
 * @param $cover string applicant cover letter
 */
function mailFunc($fileID,$id,$name,$email,$cover){
    $file_path = get_attached_file($fileID);
    $the_file = wp_get_attachment_url($fileID);
    $taxonomyCategory = wp_get_post_terms($id, 'jobman_category',1 );

    $subject = get_the_title($id ) .' - '. $taxonomyCategory[0]->name ;
    $headers = "From: " . strip_tags('no-reply@volumepeople-backend.com') . "\r\n";
    $headers .= 'Bcc:Thilina<thilina@thilina.lk>, Kashif<kashifkaleel@gmail.com>'."\r\n";
    $headers .= "MIME-Version: 1.0\r\n";
    $headers .= "Content-Type: text/html; charset=UTF-8\r\n";
    //$blogusers = get_users( array( 'fields' => array( 'user_email' ) ) );
    $message = '<html xmlns="http://www.w3.org/1999/xhtml"><head> <meta http-equiv="Content-Type" content="text/html; charset=utf-8" /> <title>VOLUME PEOPLE </title> <style type="text/css">p[class="body-text"]{font-family:Arial,Helvetica,sans-serif;font-size:14px;line-height:22px;color:#000;margin:20px 0 10px 0;text-align:left} </style></head><body><table width="100%" cellpadding="0" cellspacing="0" border="0" bgcolor="#FFFFFF" align="center"> <tr> <td> <table width="650" cellpadding="0" cellspacing="0" border="0" bgcolor="#F6F6F6" align="center"> <tr> <td valign="top"><img src="http://mailer.volume.co.uk/351234/banner-image.png" alt="" width="650" height="115" border="0" /> </td> </tr> <tr> <td style="padding:30px 50px"> <h2 style="font-family:Arial,Helvetica,sans-serif;color:#1f497d;text-align:left">A new CV arrived from Volume People.</h2> <p style="font-family:Arial,Helvetica,sans-serif;font-size:14px;line-height:22px;color:#1f497d;text-align:left"><b>Name : </b> '.$name.'</p> <p style="font-family:Arial,Helvetica,sans-serif;font-size:14px;line-height:22px;color:#1f497d;text-align:left"><b>Email : </b> '.$email.'</p> <p style="font-family:Arial,Helvetica,sans-serif;font-size:14px;line-height:22px;color:#1f497d;text-align:left"><b>Cover letter : </b> </p> <p style="font-family:Arial,Helvetica,sans-serif;font-size:14px;line-height:22px;color:#1f497d;text-align:left;background-color: #eee;padding: 5px;">'.$cover.'</p> <p style="font-family:Arial,Helvetica,sans-serif;font-size:14px;line-height:22px;color:#1f497d;text-align:left">Click the link below to get it whilst it&#39;s still hot. <p style="font-family:Arial,Helvetica,sans-serif;font-size:14px;line-height:22px;color:#000;margin:20px 0 10px 0;text-align:left"> <a href="'.$the_file.'">Download CV</a> </p> </td> </tr><td colspan="5"> <table width="650" cellpadding="0" cellspacing="0" border="0" bgcolor="#383838" align="left"> <tr> <td style="padding-left:10px"> <p style="color:#fff;font-family:Arial,Helvetica,sans-serif;font-size:12px;line-height:22px;margin:25px 10px">&copy; Volume Ltd. 2015</p> </td> <td> <p style="color:#fff;font-family:Arial,Helvetica,sans-serif;font-size:12px;line-height:22px;margin:25px 10px">&nbsp;</p> </td> <td> <p style="color:#fff;font-family:Arial,Helvetica,sans-serif;font-size:12px;line-height:22px;margin:25px 10px">&nbsp;</p> </td> <td> <p style="color:#fff;font-family:Arial,Helvetica,sans-serif;font-size:12px;line-height:22px;margin:25px 10px">&nbsp;</p> </td> <td> <p style="color:#fff;font-family:Arial,Helvetica,sans-serif;font-size:12px;line-height:22px;margin:25px 10px">&nbsp;</p> </td> </tr> <tr> <td align="right" width="350" colspan="5"> </td> </tr> </table> </td> </tr> </table> </td> </tr></table></body></html>';
    wp_mail( 'careers@volumeglobal.com', $subject, $message, $headers, $file_path );
}
