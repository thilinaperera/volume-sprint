<?php
/**
 * Template Name: Benefits
 *
 * @link
 * @since 1.0
 *
 * @package WordPress
 * @subpackage Volume People
 * @author Kashif
 */
get_header(); ?>

    <div class="benefits-main site-body">
        <div class="body-bg"></div>
        <div class="header-sub full-width" id="1">
            <div class="wrap">
                <div class="copy-area">
                    <h1>WIDE-RANGING <span>BENEFITS</span></h1>
                    <p>We invest in the wellbeing of our employees to ensure they remain happy, healthy and productive. </p>
                </div>
            </div>
        </div>
        <div class="parallax-holder">
            <div class="parallax-content">
                <div class="waypoint-wrapper">
        <div class="content-main essentials full-width" id="2">
            <div class="wrap">
                <div class="content-holder">
                    <h2>THE<span> ESSENTIALS</span></h2>
                    <hr/>
                    <ul>
                        <li>Health Shield</li>
                        <li>Personal pension</li>
                        <li>Group life assurance</li>
                        <li> Annual leave</li>
                        <li>Personal Accident & Sickness (PA&S) scheme</li>
                        <li>Unlimited training</li>
                        <li>Fruit Drop</li>
                        <li>A Bit of Stick sandwich and snack delivery service</li>
                    </ul>
                </div>
                <div class="content-holder">
                    <div class="bg-image">
                        <img src="<?php bloginfo('stylesheet_directory'); ?>/images/banana.png"  alt="">
                    </div>
                </div>
            </div>
        </div>
        <div class="content-main extras full-width" id="3">
            <div class="wrap">
                <div class="content-holder">
                    <div class="bg-image">
                        <img src="<?php bloginfo('stylesheet_directory'); ?>/images/shoes.png"  alt="">
                    </div>
                </div>
                <div class="content-holder">
                    <h2>ADDED<span> EXTRAS</span></h2>
                    <hr/>
                    <ul>
                        <li>Private Medical Insurance (PMI) scheme</li>
                        <li>Childcare vouchers</li>
                        <li>Cycle-to-work scheme</li>
                        <li>Car partner scheme</li>
                        <li>Gym membership discount</li>
                        <li>Food and drink discounts</li>
                    </ul>

                </div>
            </div>
        </div>
        <div class="content-main more full-width" id="4">
            <div class="wrap">
                <div class="content-holder">
                    <h2>and<span> more</span></h2>
                    <hr/>
                    <ul>
                        <li>World-class induction program and buddy scheme</li>
                        <li>Work placement opportunities abroad</li>
                        <li>Garden games</li>
                        <li>Yoga classes</li>
                        <li>Xbox Kinect</li>
                        <li>Dry cleaning service</li>
                        <li>Car washing service</li>
                        <li>Christmas and summer parties</li>
                        <li>TFI Fridays</li>
                        <li>Employee referral bonus</li>
                        <li>Laithwaites wine discount</li>
                        <li>Gel nail treatments</li>
                    </ul>
                </div>
                <div class="content-holder">
                    <div class="bg-image">
                        <img src="<?php bloginfo('stylesheet_directory'); ?>/images/xbox.png"  alt="">
                    </div>
                </div>
            </div>
        </div>
        <div class="clear"></div>
        <div class="content-main quote full-width" id="5">
            <div class="wrap">
                <div class="quote-holder">
                    <div class="quote-holder-wrapper">
                       <!-- <div class="open-quote"></div>
                        <p>We never stop learning. It’s a fact! That’s why we provide unlimited training to each and every Volume Person.</p>
                        <div class="close-quote"></div> -->
                        <blockquote class="open-block">
                            <p class="blockquote">We never stop learning. It&rsquo;s a fact&#33; That&rsquo;s why we provide unlimited training to each and every volume person.</p>
                        </blockquote >

<!--                        <blockquote class="close-block"></blockquote>-->
                    </div>
                </div>
            </div>
        </div>
        <div class="clear"></div>
        <div class="content-main tools full-width" id="6">
            <div class="wrap">
                <div class="content-holder">
                    <div class="bg-image">
                    </div>
                </div>
                <div class="content-holder">
                    <h2>Tools<span> and</span> training</h2>
                    <hr/>
                    <p>We never stop learning. It’s a fact! That’s why we provide unlimited online training to each and every Volume Person.</p>
                    <p>What’s more, we regularly run peer-to-peer Learn @ Lunch and Show & Tell talks – because we believe that true development involves both teaching and being taught.</p>
                </div>

                <div class="image-holder">
                </div>
            </div>
        </div>
        </div>
        <?php get_footer(); ?>
            </div><!--end paralax content-->
        </div><!--end paralax holder-->
    </div>


