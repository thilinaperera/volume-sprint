<?php
/**
 * Template Name: Company
 *
 * @link
 * @since 1.0
 *
 * @package WordPress
 * @subpackage Volume People
 * @author Kashif
 */
get_header(); ?>

    <div class="company-main site-body">
        <div class="body-bg"></div>
        <div class="header-sub full-width">
            <div class="wrap">
                <div class="copy-area">
                    <h1>Forward-thinking<span> company</span></h1>
                    <p>We invest in the wellbeing of our employees to ensure they remain happy, healthy and productive.</p>
                </div>
            </div>
        </div>

        <div class="parallax-holder">
            <div class="parallax-content">
                <div class="waypoint-wrapper">
        <div class="content-main awards full-width">
            <div class="wrap">
                <div class="avatar"></div>
                <h2 class="accolades">awards &<span> accolades</span></h2>
                <hr/>
                <p>We’re proud of our achievements – and our People are too. After all, we wouldn’t be where we are today without each and every one of them.</p>
                <p>Take a look at some of our awards and accolades below. Oh, and did we mention we&rsquo;ve been named as a <strong>two star organisation</strong> in The Sunday Times&rsquo; list of Best Companies To Work For&#63;</p>
                <div class="clear"></div>
            </div>

            <div class="mob-award-logo">
                <div class="wrap">
                    <ul>
                        <li>
                            <a id="digital" class="digital" href="http://bit.ly/1Qe7RSW" target="_blank"></a>
                        </li>
                        <li>
                            <a id="bea" class="wirehive" href="http://bit.ly/1KbRra0" target="_blank"></a>
                        </li>
                        <li>
                            <a id="thames" class="be" href="http://bit.ly/1mhIvtT" target="_blank"></a>
                        </li>
                        <li>
                            <a id="wirehive" class="bea" href="http://bit.ly/1VTLViD" target="_blank"></a>
                        </li>
                        <li>
                            <a id="be" class="thames" href="http://ibm.co/1nLkHj9" target="_blank"></a>
                        </li>
                    </ul>
                </div>
                </div>

            <div class="awards-logo">
                <div class="wrap">
                <ul>
                    <li>
                        <a id="digital" class="digital" href="http://bit.ly/1Qe7RSW" target="_blank"></a>
                        <div class="arrow-pointer digital"></div>
                    </li>
                    <li>
                        <a id="bea" class="wirehive" href="http://bit.ly/1KbRra0" target="_blank"></a>
                        <div class="arrow-pointer wirehive"></div>
                    </li>
                    <li>
                        <a id="thames" class="be" href="http://bit.ly/1mhIvtT" target="_blank"></a>
                        <div class="arrow-pointer be"></div>
                    </li>
                    <li>
                        <a id="wirehive" class="bea" href="http://bit.ly/1VTLViD" target="_blank"></a>
                        <div class="arrow-pointer bea"></div>
                    </li>
                    <li>
                        <a id="be" class="thames" href="http://ibm.co/1nLkHj9" target="_blank"></a>
                        <div class="arrow-pointer thames"></div>
                    </li>
                </ul>
            </div>

                <div class="award-detail-wrap">
                    <div class="award-detail digital">
                        <h3>Best Companies<span> &ndash; Two-Star Organisation 2016</span></h3>
                        <hr>
                       <p> It&rsquo;s easy to see why our Volume People love working with us. That&rsquo;s why Best Companies &ndash; which provides the accreditation standard to acknowledge excellence in the workplace &ndash; has named us as a two-star organisation, highlighting our outstanding employee offering. <a href="http://bit.ly/1Qe7RSW" target="_blank">http://bit.ly/1Qe7RSW</a>
                       </p>
                    </div>
                    <div class="award-detail wirehive">
                        <h3>Learning and Performance Institute<span> &ndash; Accredited Learning Technologies Provider</span></h3>
                        <hr>
                        <p>Our Learning &amp; Development Practice has been awarded the Learning &amp; Performance Institute&rsquo;s Learning Provider Accreditation. This globally recognised quality mark sets us apart, as we deliver innovative training content to some of the world&rsquo;s biggest brands. <a href="http://bit.ly/1VTLViD" target="_blank">http://bit.ly/1VTLViD</a>
                        </p>
                    </div>
                    <div class="award-detail thames">
                        <h3>Thames Valley Business Magazine Awards<span> &ndash; Best Use of Technology</span></h3>
                        <hr>
                        <p>The criteria&#58; demonstrating an innovative approach to the application of technology, leading to a measured improvement in the customer service experience whilst bringing about significant cost savings. The prize: the prestigious Gold award for Best Use of Technology. And we won&#33; <a href="http://bit.ly/1mhIvtT" target="_blank">http://bit.ly/1mhIvtT</a>
                        </p>
                    </div>
                    <div class="award-detail be">
                        <h3>IBM Watson <span> &ndash; Ecosystem Partner</span></h3>
                        <hr>
                        <p>We&rsquo;ve established the UK&rsquo;s first Centre of Excellence for Cognitive Computing and Machine Learning &ndash; Volume.XO &ndash; and are proud to announce our partnership with leading platform IBM Watson. Watch this space, as we launch some awesome cognitive applications&#33; <a href="http://ibm.co/1nLkHj9" target="_blank">http://ibm.co/1nLkHj9</a>
                        </p>
                    </div>
                    <div class="award-detail bea">
                        <h3>AI Business Excellence Awards <span> – Most Innovative Digital Media Company 2015</span></h3>
                        <hr>
                        <p>The prestigious Business Excellence Awards were launched to highlight and celebrate the outstanding performance and results achieved by the industry’s leading lights.
                            This year, we were awarded the ‘Most Innovative Digital Media Company’. <a href="http://bit.ly/1M7kj5R" target="_blank">http://bit.ly/1M7kj5R</a>
                        </p>
                    </div>

                </div>
            </div>
        </div>
        <div class="content-main heritage full-width">
            <div class="bg-wrapper"></div>
            <div class="wrap">
                <div class="content-holder">
                    <div class="content-wrapper">
                    <h2>our<span> heritage</span></h2>
                    <hr/>
                    <p>Like many legendary tales, ours starts with a boy and his dream. And, unlike the vast majority, a bottle of Malibu.</p>
                    <p>From our humble beginnings in 1997, we’ve come a long way – designing and developing our own digital campus; building a talented team; and growing globally.</p>
                    <p>We love what we do, and our clients do too… in fact, several have been with us since day one. Now is your opportunity to be a part of our story and our future success!</p>
                    </div>
                </div>
                <div class="content-holder">
                </div>

                <div class="bottle">

                </div>
            </div>
            <div class="clear"></div>
        </div>
        <div class="content-main values full-width">
            <div class="wrap">
                <div class="content-holder">

                </div>
                <div class="content-holder content">
                    <h2>Our<span> Values and beliefs</span></h2>
                    <hr/>
                    <p>At Volume, excellence isn’t something we strive for; it’s something we’ve come to expect from every employee. Going above and beyond is a given, and the aspiration to be world-class is in our DNA.</p>
                    <p>We believe that in providing our Volume People with outstanding experiences – from training and development to rewards and recognition – we build a relationship of mutual respect and trust that drives our business forward.</p>
                </div>
                <div class="image-holder">
                    <!--<img src="<?php bloginfo('stylesheet_directory'); ?>/images/company-values.jpg"  alt="">-->
                </div>
            </div>
        </div>
        <div class="content-main quote full-width">
            <div class="wrap">
                <div class="quote-holder">
                    <!-- <div class="open-quote"></div>
                    <p>If we hire you, we admire you. Simple!</p>
                    <div class="close-quote"></div> -->
                    <div class="quote-holder-wrapper">
                        <blockquote class="open-block"></blockquote >
                        <p class="blockquote">If we hire you, we admire you. Simple&#33;</p>
                        <blockquote class="close-block"></blockquote>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-main offices full-width">
            <div class="location-list full-width">
                <div class="wrap">
                    <h2>our<span> offices</span></h2>
                    <hr/>
                    <div class="our-office">
                    </div>
                    <ul>
                        <li>
                            <!--<div class="location">
                                <a class="location-holder workingham" data-region-id="#Wokingham">
                                </a>
                            </div>-->
                            <h4>Volume Digital Campus:<br> <strong>Wokingham, UK </strong></h4>
                            <p>Situated in leafy Wokingham, Berkshire – just a stone’s throw from London – Volume’s Digital Campus boasts an innovative open-plan layout. There are no barriers here, with Account Executives and Artworkers sitting alongside our HR and Senior Management teams.</p>
                        </li>
                        <li>
                            <!--<div class="location">
                                <a class="location-holder plymouth" data-region-id="#Plymouth">
                                </a>
                            </div>-->
                            <h4>Volume L&D Centre:<br> <strong>Plymouth, UK</strong></h4>
                            <p>Delightful Devon is home to our Learning & Development Centre. Our People love working in a beautiful Grade II listed building, fitted out with the modern touches you can expect from Volume.</p>
                        </li>
                        <li>
                            <!--<div class="location">
                                <a class="location-holder plymouth" data-region-id="#Kingsbridge">
                                </a>
                            </div>-->
                            <h4>Volume.XO HQ:<br> <strong>London, UK</strong></h4>
                            <p>Our shiny new office in the big city is our cognitive hub that provides our people with more opportunities to meet clients and make connections.</p>
                        </li>
                        <li>
                            <!--<div class="location">
                                <a class="location-holder colombo" data-region-id="#Colombo">
                                </a>
                            </div>-->
                            <h4>Volume Technology Centre:<br> <strong>Colombo, Sri Lanka</strong></h4>
                            <p>Many of our talented techies work from Sri Lanka’s prestigious World Trade Center. When they’re not building apps or pepping up Content Management platforms, you’ll find them playing table football!</p>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        </div>
        <?php get_footer(); ?>
            </div><!--end paralax content-->
        </div><!--end paralax holder-->

</div>


