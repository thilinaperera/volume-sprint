<?php get_header(); ?>
    <div class="careers-main site-body shared">
        <div id="1" class="header-sub full-width" id="1">
            <div class="wrap">
                <div class="copy-area">
                    <h3>Career-Boosting<span> Opportunities</span></h3>

                    <p>Join our journey and work with some of the world’s biggest and best brands.</p>
                </div>
            </div>
        </div>
       <div class="featured-job full-width" id="2">
          <div class="wrap">
          <?php
        $taxonomyCategory = wp_get_post_terms(get_the_ID(), 'jobman_category', 1);
        $title = ' - ' . $taxonomyCategory[0]->name; ?>
            <h1 class="share-job-location"><?= get_post_meta(get_the_ID(),'data4',true) ; ?><?= $title ?></h1>
            <hr class="share-location-hr">
             <div class="job-detail-wrap">
             <div class="job-detail-holder">
              <div class="icon technology "></div>
                <div class="job-detail">

                <h2 class="job-title-head"><?=  get_the_title( get_the_ID()); ?></h2>
                <div class="job-detail-copy"><?= get_post_meta(get_the_ID(),'data5',true) ; ?></div>
                </div>
                </div>
                <div class="jobdetail-bottom job-detail-holder">
                    <div class="bottom-content uploadCV">
                        <h3>apply now</h3>
                        <h4>I would like to apply for the role of <?=  get_the_title( get_the_ID()); ?> based in <?= get_post_meta(get_the_ID(),'data4',true) ; ?><?= $title ?>.</h4>

                        <div class="message-block">
                                                 <span class="icon">
                                                 </span>
                            <label></label>
                        </div>
                        <div class="clear"></div>
                        <form id="cvUploderForm" action="" method="post" name="test" enctype="multipart/form-data">
                              <?php


                            $taxonomyCategory = wp_get_post_terms(get_the_ID(), 'jobman_category',1 );
                            $meatas = get_post_meta(get_the_ID() );
                            $options = get_option( 'jobman_options' );

                            ?>
                            <input class="jobid" type="hidden" name="jobid" value="<?=  get_the_ID() ?>"/>
                            <input class="c_slug" type="hidden" name="c_slug" value="<?= $taxonomyCategory[0]->slug ?>"/>
                            <input type="hidden" name="data5" value=""/>
                            <ul>
                                <li><span class="special-label">Full Name:</span>
                                    <input type="text" name="name" placeholder="Your name" required></li>
                                <li>
                                    <span class="special-label">Email:</span>
                                    <input type="email" name="email" placeholder="Your email address" required></li>
                                <li>
                                    <span class="special-label">Cover Letter:</span>
                                    <textarea name="cover" cols="30" rows="10" placeholder="A few words about yourself" required></textarea></li>
                            </ul>
                            <div class="notify-block">
                                <label></label>
                            </div>
                            <div class="progress-block">
                                <div class="loader"></div>
                            </div>
                            <div class="fileListHolder">
                                <ul class="fileList"></ul>
                            </div>
                            <div class="uploader drag-and-drop-zone">
                                <div class="browser">
                                    <label>
                                        <div class="drop-icon"></div>
                                        <div class="upload-title">DROP YOUR <strong>CV</strong> HERE</div>
                                        <div class="file-formats">(.doc, docx, pdf formats are allowed. You can attach only one document.)</div>
                                        <input type="file" name="cv_file" title="Click to add Files">
                                    </label>
                                </div>
                            </div>
                            <input type="submit" name="submit"  class="button-primary" value="submit" />
                        </form>
                    </div>
                    <div class="bottom-content share">
                        <h3>share now</h3>
                         <ul>
                          <li>
                              <a target="_blank" class="fb" title="Share" href="http://www.facebook.com/sharer.php?u=<?php echo urlencode(get_permalink($wokingham_recent->ID )); ?>">

                              </a>
                          </li>
                          <li>
                              <a target="_blank" class="twitter" title="Tweet" href="https://twitter.com/share?url=<?php echo urlencode(get_permalink($wokingham_recent->ID )); ?>">

                              </a>
                          </li>
                          <li>
                              <a target="_blank" class="linkedin"  title="Share" href="http://www.linkedin.com/shareArticle?mini=true&url=<?php echo urlencode(get_permalink($wokingham_recent->ID )); ?>">

                              </a>
                          </li>
                          <li>
                              <a target="_blank" class="mail"  title="Share" href="mailto:jobs@volumepeople.com?subject=<?=  get_the_title( get_the_ID()); ?>">
                              </a>
                          </li>
                      </ul>
                  </li>
                    </div>


                </div>
            </div>
        </div>
    </div>
</div>

    <script>

        var jobList = <?= json_encode($jsArray) ?>;

        // variables for job upload
        var jobID = <?=  get_the_ID() ?>;
        var slug = "<?= $taxonomyCategory[0]->slug ?>";
        var attachments = true;
        var attachment = 0;
        $(".drag-and-drop-zone").dmUploader({
            url: '<?= THEMEROOT ?>/ajax-uploader.php',
            maxFileSize: 52428800,
            extraData: {
                'jobID': 'no need',
                'c_slug': 'no need',
                'attachments' : attachments
            },
            extFilter: 'pdf;doc;docx',
            //maxFiles: 1,
            onBeforeUpload: function (id) {
                console.log('Starting to upload #' + id);
            },
            onComplete: function () {
                console.log('We reach the end of the upload Queue!');
            },
            onUploadProgress: function (id, percent) {
                console.log('Upload of #' + id + ' is at %' + percent);
                // do something cool here!
                $('.uploadCV .progress-block').show();
                $('.uploadCV .progress-block .loader').width(percent+'%');
            },
            onUploadSuccess: function (id, data) {

                var data = JSON.parse(data);
                console.log(data);

                /**
                 * Add new attachment to the attachments array
                 */
                if(data.code == 202){
                    $('.uploadCV .progress-block .loader').css({
                        //'background-color': '#95cf99'
                        'background-color': 'transparent'
                    });
                    $('.uploadCV .progress-block').hide();
                    attachment = data.file;
                    attachments = false;
                    var holderElem = $('.uploadCV .fileListHolder .fileList');
                    var fileElem = $('<li class="fileLinkHolder"><a title="View file" target="_blank" href="'+data.url+'">'+data.name+'</a></li>');
                    var link = $('<span data="'+data.file+'" class="closeIcon" title="Remove file"><img src="<?= IMAGES ?>/file.png" width="50" height="auto"></span>');
                    holderElem.append(fileElem).delay(10,function(){


                    });
                    fileElem.append(link);
                    link.click(function(){
                        $('.uploadCV .notify-block')
                            .hide()
                            .children('label').html("");
                        $('.uploadCV .progress-block').show();
                        $('.uploadCV .progress-block .loader')
                            .width(0)
                            .css({
                                'background-color': '#95cf99'
                            });

                        $.ajax({
                            method: "POST",
                            url: '<?= THEMEROOT ?>/ajax-uploader.php',
                            data: {
                                action: 'delete',
                                data : {
                                    id : $(this).attr('data')
                                }
                            }
                        })
                            .done(function( result ) {
                                var result = JSON.parse(result);
                                if(result.code == 200){

                                    fileElem.remove();
                                    attachments = true;
                                    attachment = false;
                                    $('.uploadCV .notify-block').hide();
                                }else{
                                    console.log('sever error')
                                }
                            });

                    });
                    $('.uploadCV .notify-block').hide();

                }else if(data.code == 406){
                    //alert('please remove existing file')
                    console.log(attachments);

                    //$('.fancybox-opened .uploadCV .progress-block').hide();
                    //$(".nano").nanoScroller({ scrollTo: $('.notify-block')});
                    $('.uploadCV .notify-block').show();
                    $('.uploadCV .notify-block label').text('You can only upload one document. The following document is attached to your application.');


                }else{
                    //alert('server error')
                }




            },
            onUploadError: function (id, message) {
                console.log('Error trying to upload #' + id + ': ' + message);
            },
//            onFileTypeError: function(file){
//                console.log('File type of ' + file.name + ' is not allowed: ' + file.type);
//            },
            onFileExtError: function (file) {
                //console.log('File extension of ' + file.name + ' is not allowed');
                $('.uploadCV .notify-block').show();
                $('.uploadCV .notify-block').addClass('validation');
                $('.uploadCV .notify-block label').text('');
                $('.uploadCV .notify-block label').text('File extension of ' + file.name + ' is not allowed. Only .doc, .docx, .pdf formats are allowed.');


            },
            onFilesMaxError: function (file) {
                $('.uploadCV .message-block').removeClass('progress');
                $('.uploadCV .message-block').removeClass('validation');
                $('.uploadCV .message-block label').text(' ');
                $('.uploadCV .message-block').addClass('success');
                $('.uploadCV .message-block label').text('You have already applied to this position.');
                console.log('You have already applied to this position.');
                //$(".nano").nanoScroller({ scroll: 'top' });
            }
        });

        var options = {
            beforeSubmit:  showRequest,  // pre-submit callback
            success:       showResponse,  // post-submit callback
            data: {
                action: 'submit',
                data : {
                    job : jobID,
                    slug : slug,
                    attachment : attachment
                }
            },
            url: '<?= THEMEROOT ?>/ajax-uploader.php',
            dataType:  'json',        // 'xml', 'script', or 'json' (expected server response type)
            clearForm: true,        // clear all form fields after successful submit
            resetForm: true      // reset the form after successful submit
        };


        /**
         * Validation
         */
        $("#cvUploderForm").validate({
            submitHandler: function(form) {
                if(!attachment){
                    //alert('please upload ur cv');
                    $('.uploadCV .notify-block').show();
                    $('.uploadCV .notify-block').addClass('validation');
                    $('.uploadCV .notify-block label').text('');
                    $('.uploadCV .notify-block label').text('Please upload your CV.');
                    return false;
                }
                $(form).ajaxSubmit(options)
            },
            invalidHandler : function () {
                //$('.job-overlay .bottom-content.uploadCV .message-required').show();
                //$(".nano").nanoScroller({ scroll: 'top' });
            }
        });


        /**
         * Trigger before submit ajax call
         * @param formData
         * @param jqForm
         * @param options
         * @returns {boolean}
         */
        function showRequest(formData, jqForm, options) {
            // formData is an array; here we use $.param to convert it to a string to display it
            // but the form plugin does this for you automatically when it submits the data
            var queryString = $.param(formData);

            // jqForm is a jQuery object encapsulating the form element.  To access the
            // DOM element for the form do this:
            // var formElement = jqForm[0];

            //alert('About to submit: \n\n' + queryString);
            return true;
        }

        /**
         * Ajax request callback
         * @param responseText
         * @param statusText
         * @param xhr
         * @param $form
         */
        function showResponse(responseText, statusText, xhr, $form)  {
            attachments = true;
            attachment = false;
            var holderElem = $('.uploadCV .fileListHolder .fileList');
            holderElem.html("");
            //alert('status: ' + statusText + '\n\nresponseText: \n' + JSON.stringify(responseText) +
            //'\n\nThe output div should have already been updated with the responseText.');
            $('.uploadCV .notify-block').hide();
            $('.uploadCV .message-block').show();
            $('.uploadCV .message-block').addClass('success');
            $('.uploadCV .message-block label').text('Thank you for your CV, we will review and contact you as soon as we can.');
            $('html, body').animate({
                scrollTop: $(".bottom-content.uploadCV").offset().top
            }, 1000);
        }

        $(document).ready(function(){
            if($('.careers-main.shared').length > 0) {
                $('.home-header').addClass('careers-single');
            }

            //add a class for IE specific versions
            //add a class for IE specific versions
            if (/MSIE (\d+\.\d+);/.test(navigator.userAgent))
            {
                var ieversion=new Number(RegExp.$1);
                if (ieversion == 9) {
                    $('body').addClass("ie9");
                }
            }
        });
    </script>



<?php get_footer(); ?>