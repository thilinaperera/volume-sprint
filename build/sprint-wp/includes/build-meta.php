<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
<meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
<!-- END Required Meta Tags -->
<title><?php wp_title(); ?></title>
<?php
    $taxonomyCategory = wp_get_post_terms(get_the_ID(), 'jobman_category', 1);
    $post_type = get_post_type($post->ID);
    $post_type == 'jobman_job'; ?>
    <meta property="og:title" content="<?php the_title().' - ' . $taxonomyCategory[0]->name; ?>"/>
    <meta property="og:type" content="website"/>
    <meta property="og:image" content="<?php bloginfo('stylesheet_directory'); ?>/images/slide-5.jpg"/>
    <meta property="og:url" content="<?= get_the_permalink() ?>"/>
    <meta property="og:description" content="We’re growing – and fast! That’s because we believe the only way to be the best is to hire the best. Again and again."/>

    <meta name="twitter:url" content="<?= get_the_permalink() ?>"/>
    <meta name="twitter:title" content="<?php the_title(); ?><?= $title ?>"/>
    <meta name="twitter:description" content="We’re growing – and fast! That’s because we believe the only way to be the best is to hire the best. Again and again."/>
    <meta name="twitter:image:src" content="<?php bloginfo('stylesheet_directory'); ?>/images/slide-5.jpg"/>





    <!-- Begin Favicons -->
    <link href="<?php echo get_stylesheet_directory_uri(); ?>/images/favicon.ico" rel="shortcut icon"/>
    <link href="<?php echo get_stylesheet_directory_uri(); ?>/images/favicon.png" rel="apple-touch-icon"/>
    <!-- End Favicons -->

<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-5272842-36', 'auto');
    ga('send', 'pageview');

</script>