<?php
/**
 * Template Name: Careers
 *
 * @link
 * @since 1.0
 *
 * @package WordPress
 * @subpackage Volume People
 * @author Kashif
 */
get_header(); ?>

<div class="careers-main site-body">
    <div class="body-bg"></div>
    <div class="header-sub full-width" id="1">
        <div class="wrap">
            <div class="copy-area">
                <h1>Career-Boosting<span> Opportunities</span></h1>

                <p>Join our journey and work with some of the world’s biggest and best brands.</p>
            </div>
        </div>
    </div>
<div class="parallax-holder">
<div class="parallax-content">
    <div class="waypoint-wrapper">
    <div class="intro full-width" id="2">
        <div class="wrap">
            <h2>Current<span> Vacancies</span></h2>
            <hr/>
            <p class="mob-potr-show">We’re growing – and fast! That’s because we believe the only way to be the best is to hire the
                best. Again and again.</p>

            <p>If you’d like to grow with us, grow your knowledge, grow your experience and grow new friendships,
                take a look at our live roles below:</p>

            <p>Questions, concerns or just fancy a chat? Please email <a href="mailto:careers@volumeglobal.com?subject=I am interested in joining Volume">
                    careers@volumeglobal.com</a> and our friendly HR team will get back to you within 24 hours.</p>
            <p>What&rsquo;s more, if you&rsquo;ve got a friend who&rsquo;d be a perfect fit, we&rsquo;ve got &pound;1,000 up for grabs (but don&rsquo;t forget to read the  <a href="javascript:$.fancybox($('.small-print').show())" class="fancyBoxSmallPrint">small print</a> first).</p>
            <div style="display: none" class="small-print">
                <div id="content-div">
                    <div class="small-print-header">
                        <h3>Referral Scheme Terms &amp; Conditions</h3>
                    </div>
                    <ul>
                        <li>The reward is payable in the month that the person referred passes their probationary period at Volume (typically three months after the start date, unless extended).</li>
                        <li>The reward is payable via BACs to the bank account details provided.</li>
                        <li>Upon receipt of the referral reward we ask that you declare this to the HMRC and pay any liabilities that may be due thereafter.</li>
                        <li>To successfully refer someone for one of Volume&rsquo;s vacancies, the referrer must email <a href="mailto:careers@volumeglobal.com">careers@volumeglobal.com</a> with the referee&rsquo;s CV and personal email address.</li>
                        <li>This reward is only applicable to candidates who have not been introduced to Volume within the 24 months prior.</li>
                        <li>This reward offer may be amended or withdrawn at any time.</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>


    <?php


    $args = array('post_type' => 'jobman_job', 'posts_per_page' => -1);
    $the_query = new WP_Query($args);

    ?>



    <?php

    $jsArray = array();

    $wokingham = array();
    $plymouth = array();
    $colombo = array();

    if (have_posts()) :
        while ($the_query->have_posts()) : $the_query->the_post();

            $location = get_post_meta(get_the_ID(), 'data4', true);
            //echo json_encode($location);
            $taxonomyCategory = wp_get_post_terms(get_the_ID(), 'jobman_category', 1);
            $temp = array(
                'id' => get_the_ID(),
                'title' => get_the_title(get_the_ID()),
                'term_slug' => $taxonomyCategory[0]->slug,
                'term_name' => $taxonomyCategory[0]->name,
                'description' => get_post_meta(get_the_ID(), 'data5', true),
                'location' => $location,
                'department' => get_post_meta(get_the_ID(), 'data6', true),
                'dep_slug' => clean(get_post_meta(get_the_ID(), 'data6', true)),
                'share_url' =>  urlencode(get_permalink(get_the_ID()))
            );
            array_push($jsArray, $temp);
            if ($location == 'Colombo') {
                array_push($colombo, $post);
            }
            if ($location == 'Wokingham') {
                array_push($wokingham, $post);
            }
            if ($location == 'plymouth') {
                array_push($plymouth, $post);
            }
            //echo  '<br/><br/>';
        endwhile;

        //echo json_encode($colombo);
        //get_post


    endif;
    ?>

    <div class="vacancy-list full-width" id="3">
        <div class="location-list full-width">
            <div class="wrap">
                <ul>
                    <li>
                        <div class="location selected">

                            <a class="location-holder workingham <?=  count($wokingham) ? "" : "no-opportunity"  ?>" data-region-id="#Wokingham">
                                <div class="border-wrap">
                                <div class="location-name"><span>Wokingham, UK</span></div>
                                    <?php   $count = count($wokingham);
                                       if ( $count ==1 ) { ?>
                                <div class="job-count"><span><?php echo $count; ?> OPPORTUNITY</span></div>
                                     <?php } elseif ( $count ==0 ) { ?>
                                <div class="job-count"><span><?php echo $count; ?> OPPORTUNITIES</span></div>
                                    <?php } else { ?>
                                <div class="job-count"><span><?php echo $count; ?> OPPORTUNITIES</span></div>

                                    <?php } ?>
                                </div>
                            </a>
                        </div>
                    </li>
                    <li>
                        <div class="location">
                            <a class="location-holder kingsbridge <?=  count($plymouth) ? "" : "no-opportunity"  ?>" data-region-id="#Plymouth">
                                <div class="border-wrap">
                                <div class="location-name"><span>Plymouth, UK</span></div>
                                  <?php   $count = count($plymouth);
                                       if ( $count ==1 ) { ?>
                                <div class="job-count"><span><?php echo $count; ?> OPPORTUNITY</span></div>
                                     <?php } elseif ( $count ==0 ) { ?>
                                <div class="job-count"><span><?php echo $count; ?> OPPORTUNITIES</span></div>
                                    <?php } else { ?>
                                <div class="job-count"><span><?php echo $count; ?> OPPORTUNITIES</span></div>

                                    <?php } ?>
                                </div>
                            </a>


                        </div>
                    </li>
                    <li>
                        <div class="location">
                            <a class="location-holder colombo <?=  count($colombo) ? "" : "no-opportunity"  ?>" data-region-id="#Colombo">
                                <div class="border-wrap">
                                    <div class="location-name"><span>Colombo, Sri Lanka</span></div>
                                      <?php   $count = count($colombo);
                                       if ( $count ==1 ) { ?>
                                <div class="job-count"><span><?php echo $count; ?> OPPORTUNITY</span></div>
                                     <?php } elseif ( $count ==0 ) { ?>
                                <div class="job-count"><span><?php echo $count; ?> OPPORTUNITIES</span></div>
                                    <?php } else { ?>
                                <div class="job-count"><span><?php echo $count; ?> OPPORTUNITIES</span></div>

                                    <?php } ?>
                                </div>
                            </a>


                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <?php
    $length = count($wokingham);
    $left = ($length / 2) + ($length % 2);
    $right = $length / 2;

    if ($length % 2 == 1) {
        $left = (($length - 1) / 2) + 1;
        $right = ($length - 1) / 2;
    } else {
        $left = $length / 2;
        $right = $length / 2;
    }
    ?>

    <?php if ($length > 0) { ?>

<div class="vacant-show">
    <div class="absolute-bg">
        <div id="pager">
            <a href="#" class="">1</a>
            <a href="#" class="">2</a>
            <a href="#" class="">3</a>
        </div>

        <a class="page-button" id="prev">Prev</a>
        <a class="page-button" id="next">next</a>
    </div>


    <div class="featured-job full-width vacant-content" id="Wokingham">
        <div class="wrap">
            <h2 class="location">Wokingham, <span>UK</span></h2>
            <span class="no-opp"> <?php   $count = count($wokingham);
                if ( $count ==1 ) { ?>
                   <?php echo $count; ?> OPPORTUNITY
                <?php } elseif ( $count ==0 ) { ?>
                  <?php echo $count; ?> OPPORTUNITIES
                <?php } else { ?>
                    <?php echo $count; ?> OPPORTUNITIES

                <?php } ?></span>
        <div class="clear"></div>
        <hr>

            <h3 class="more-jobs">More Vacancies in Wokingham, <span>UK</span></h3>

            <div class="job-list">

                <ul class="list-holder">

                    <?php
                    $i = 0;
                    for ($c = 0; $c < $left; $c++) {
                        $job = $wokingham[$c];

                        $taxonomyCategory = wp_get_post_terms($job->ID, 'jobman_category', 1);
                        $meatas = get_post_meta($job->ID);
                        $options = get_option('jobman_options');

                        ?>
                        <li class="<?= oeChecker($i) ?>">

                            <div class="job-list-item">
                                <div class="job-title-header">
                                    <span class="icon <?= clean(get_post_meta($job->ID, 'data6', true)); ?>"></span>
                                    <a class="fancyBox cta-mob-potr" data="<?= $job->ID ?>" jname="<?=  get_the_title($job->ID); ?>" jlocation="Wokingham" href="#joblist-overlay"><span><?= get_the_title($job->ID); ?></span></a>
                                    <h3 class="job-title"><?= get_the_title($job->ID); ?></h3>
                                </div>
                                <div class="job-detail-sect">
                                    <p class="job-summary"> <?= wp_trim_words( get_post_meta($job->ID,'data5',true), 25, '...' ); ?></p>
                                    <a class="fancyBox job-list-cta" data="<?= $job->ID ?>" jname="<?=  get_the_title($job->ID); ?>" jlocation="Wokingham" href="#joblist-overlay">More</a>
                                    <div class="clear"></div>
                                </div>
                                <a class="fancyBox job-list-cta mobile" style="display: none" data="<?= $job->ID ?>" jname="<?=  get_the_title($job->ID); ?>" jlocation="Wokingham" href="#joblist-overlay">More</a>
                            </div>

                        </li>

                        <?php
                        $i++;
                    }
                    ?>


                </ul>


                <ul class="list-holder">


                    <?php
                    $i = 0;
                    $c = 0;
                    if($left == $right){
                        $c = $right;
                    }else{
                        $c = $right+1;
                    }

                    for ($c; $c < $length; $c++) {
                        if ($c == 0) {
                            break;
                        }
                        $job = $wokingham[$c];
                        $taxonomyCategory = wp_get_post_terms($job->ID, 'jobman_category', 1);
                        $meatas = get_post_meta($job->ID);
                        $options = get_option('jobman_options');
                        ?>
                        <li class="<?= oeChecker($i) ?>">
                            <div class="job-list-item">
                                <div class="job-title-header">
                                <span class="icon <?= clean(get_post_meta($job->ID, 'data6', true)); ?>"></span>
                                <a class="fancyBox cta-mob-potr" data="<?= $job->ID ?>" jname="<?=  get_the_title($job->ID); ?>" jlocation="Wokingham" href="#joblist-overlay"><span><?= get_the_title($job->ID); ?></span></a>
                                <h3 class="job-title"><?= get_the_title($job->ID); ?></h3>
                                </div>
                                <div class="job-detail-sect">
                                    <p class="job-summary"> <?= wp_trim_words( get_post_meta($job->ID,'data5',true), 25, '...' ); ?></p>
                                    <a class="fancyBox job-list-cta" data="<?= $job->ID ?>" jname="<?=  get_the_title($job->ID); ?>" jlocation="Wokingham" href="#joblist-overlay">More</a>
                                    <div class="clear"></div>
                                </div>
                                <a class="fancyBox job-list-cta mobile" style="display: none" data="<?= $job->ID ?>" jname="<?=  get_the_title($job->ID); ?>" jlocation="Wokingham" href="#joblist-overlay">More</a>
                            </div>
                        </li>

                        <?php
                        $i++;
                    }
                    ?>


                </ul>

            </div> <!--list end-->

        <div class="clear"></div>
        </div>

    </div>

    <?php } ?>


    <?php
    $length = count($plymouth);
    $left = ($length / 2) + ($length % 2);
    $right = $length / 2;
    if ($length % 2 == 1) {
        $left = (($length - 1) / 2) + 1;
        $right = ($length - 1) / 2;
    } else {
        $left = $length / 2;
        $right = $length / 2;
    }
    ?>

    <?php

    if ( $length > 0)
    { ?>

        <div class="featured-job full-width vacant-content" id="Plymouth">
        <div class="wrap">
            <h2 class="location">Plymouth, <span>UK</span></h2>
            <span class="no-opp"> <?php   $count = count($plymouth);
                if ( $count ==1 ) { ?>
                    <?php echo $count; ?> OPPORTUNITY
                <?php } elseif ( $count ==0 ) { ?>
                    <?php echo $count; ?> OPPORTUNITIES
                <?php } else { ?>
                    <?php echo $count; ?> OPPORTUNITIES

                <?php } ?></span>
            <div class="clear"></div>
            <hr>

            <?php if ($length > 0) { ?>
                <h3 class="more-jobs">More Vacancies in Plymouth, <span>UK</span></h3>

                <div class="job-list">
                    <ul class="list-holder">

                        <?php
                        $i = 0;
                        for ($c = 0; $c < $left; $c++) {
                            $job = $plymouth[$c];
                            $taxonomyCategory = wp_get_post_terms($job->ID, 'jobman_category', 1);
                            $meatas = get_post_meta($job->ID);
                            $options = get_option('jobman_options');
                            ?>
                            <li class="<?= oeChecker($i) ?>">
                                <div class="job-list-item">
                                    <div class="job-title-header">
                                    <span class="icon <?= clean(get_post_meta($job->ID, 'data6', true)); ?>"></span>
                                        <a class="fancyBox cta-mob-potr" data="<?= $job->ID ?>" jname="<?=  get_the_title($job->ID); ?>" jlocation="Kingsbridge" href="#joblist-overlay"<span>><?= get_the_title($job->ID); ?></span></a>
                                    <h3 class="job-title"><?= get_the_title($job->ID); ?></h3>
                                    </div>
                                    <div class="job-detail-sect">
                                        <p class="job-summary"> <?= wp_trim_words( get_post_meta($job->ID,'data5',true), 25, '...' ); ?></p>
                                        <a class="fancyBox job-list-cta" data="<?= $job->ID ?>" jname="<?=  get_the_title($job->ID); ?>" jlocation="Kingsbridge" href="#joblist-overlay">More</a>
                                        <div class="clear"></div>
                                    </div>
                                    <a class="fancyBox job-list-cta mobile" style="display: none" data="<?= $job->ID ?>" jname="<?=  get_the_title($job->ID); ?>" jlocation="Kingsbridge" href="#joblist-overlay">More</a>
                                </div>
                            </li>

                            <?php
                            $i++;
                        }
                        ?>


                    </ul>

                    <ul class="list-holder">
                        <?php
                        $i = 0;
                        $c = 0;
                        if($left == $right){
                            $c = $right;
                        }else{
                            $c = $right+1;
                        }

                        for ($c; $c < $length; $c++) {
                            if ($c == 0) {
                                break;
                            }
                            $job = $plymouth[$c];
                            $taxonomyCategory = wp_get_post_terms($job->ID, 'jobman_category', 1);
                            $meatas = get_post_meta($job->ID);
                            $options = get_option('jobman_options');
                            ?>
                            <li class="<?= oeChecker($i) ?>">
                                <div class="job-list-item">
                                    <div class="job-title-header">
                                    <span class="icon <?= clean(get_post_meta($job->ID, 'data6', true)); ?>"></span>
                                    <h3 class="job-title"><?= get_the_title($job->ID); ?></h3>
                                    </div>
                                    <div class="job-detail-sect">
                                        <p class="job-summary"> <?= wp_trim_words( get_post_meta($job->ID,'data5',true), 25, '...' ); ?></p>
                                        <a class="fancyBox job-list-cta" data="<?= $job->ID ?>" jname="<?=  get_the_title($job->ID); ?>" jlocation="Kingsbridge" href="#joblist-overlay">More</a>
                                        <div class="clear"></div>
                                    </div>
                                    <a class="fancyBox job-list-cta mobile" style="display: none" data="<?= $job->ID ?>" jname="<?=  get_the_title($job->ID); ?>" jlocation="Kingsbridge" href="#joblist-overlay">More</a>
                                </div>
                            </li>

                            <?php
                            $i++;
                        }
                        ?>


                    </ul>

                </div>

                <!--     Closing JOb List       --><?php } ?>
        </div>
    </div>


   <?php } ?>


    <?php
    $length = count($colombo);
    $left = ($length / 2) + ($length % 2);

    $right = $length / 2;

    if ($length % 2 == 1) {
        $left = (($length - 1) / 2) + 1;
        $right = ($length - 1) / 2;
    } else {
        $left = $length / 2;
        $right = $length / 2;
    }

    ?>

    <?php if ($length > 0) { ?>


    <div class="featured-job full-width vacant-content" id="Colombo">
        <div class="wrap">
            <h2 class="location">Colombo, <span>Sri Lanka</span></h2>
            <span class="no-opp"> <?php   $count = count($colombo);
                if ( $count ==1 ) { ?>
                    <?php echo $count; ?> OPPORTUNITY
                <?php } elseif ( $count ==0 ) { ?>
                    <?php echo $count; ?> OPPORTUNITIES
                <?php } else { ?>
                    <?php echo $count; ?> OPPORTUNITIES

                <?php } ?></span>
            <hr>

            <div class="clear"></div>
            <hr>


                <h3 class="more-jobs">More Vacancies in Colombo,<span> Sri Lanka</span></h3>

                <div class="job-list">

                    <ul class="list-holder">

                        <?php
                        $i = 0;
                        for ($c = 0; $c < $left; $c++) {
                            $job = $colombo[$c];
                            $taxonomyCategory = wp_get_post_terms($job->ID, 'jobman_category', 1);
                            $meatas = get_post_meta($job->ID);
                            $options = get_option('jobman_options');
                            ?>
                            <li class="<?= oeChecker($i) ?>">
                                <div class="job-list-item">
                                    <div class="job-title-header">
                                        <span class="icon <?= clean(get_post_meta($job->ID, 'data6', true)); ?>"></span>
                                        <a class="fancyBox cta-mob-potr" data="<?= $job->ID ?>" jname="<?=  get_the_title($job->ID); ?>" jlocation="Wokingham" href="#joblist-overlay"><span><?= get_the_title($job->ID); ?></span></a>
                                        <h3 class="job-title"><?= get_the_title($job->ID); ?></h3>
                                    </div>
                                    <div class="job-detail-sect">

                                        <p class="job-summary"> <?= wp_trim_words( get_post_meta($job->ID,'data5',true), 25, '...' ); ?></p>
                                        <a class="fancyBox job-list-cta" data="<?= $job->ID ?>" jname="<?=  get_the_title($job->ID); ?>" jlocation="Colombo" jcountry="Sri Lanka" href="#joblist-overlay">More</a>
                                        <div class="clear"></div>
                                    </div>
                                </div>
                            </li>

                            <?php
                            $i++;
                        }
                        ?>


                    </ul>
                    <ul class="list-holder">


                        <?php
                        $i = 0;
                        $c = 0;
                        if($left == $right){
                            $c = $right;
                        }else{
                            $c = $right+1;
                        }

                        for ($c; $c < $length; $c++) {
                            if ($c == 0) {
                                break;
                            }
                            $job = $colombo[$c];
                            $taxonomyCategory = wp_get_post_terms($job->ID, 'jobman_category', 1);
                            $meatas = get_post_meta($job->ID);
                            $options = get_option('jobman_options');
                            ?>
                            <li class="<?= oeChecker($i) ?>">
                                <div class="job-list-item">
                                    <div class="job-title-header">
                                        <span class="icon <?= clean(get_post_meta($job->ID, 'data6', true)); ?>"></span>
                                        <a class="fancyBox cta-mob-potr" data="<?= $job->ID ?>" jname="<?=  get_the_title($job->ID); ?>" jlocation="Wokingham" href="#joblist-overlay"><span><?= get_the_title($job->ID); ?></span></a>
                                        <h3 class="job-title"><?= get_the_title($job->ID); ?></h3>
                                    </div>
                                    <div class="job-detail-sect">
                                        <p class="job-summary"> <?= wp_trim_words( get_post_meta($job->ID,'data5',true), 25, '...' ); ?></p>
                                        <a class="fancyBox job-list-cta" data="<?= $job->ID ?>" jname="<?=  get_the_title($job->ID); ?>" jlocation="Colombo" jCountry="Sri Lanka" href="#joblist-overlay">More</a>
                                        <div class="clear"></div>
                                    </div>
                                </div>
                            </li>
                            <?php
                            $i++;
                        }
                        ?>


                    </ul>

                </div> <!-- Closing list -->

        </div>
    </div>
    <?php } ?>



</div>
    <!--colombo-->

        <div class="careers-bottom full-width" id="4">
        <div class="wrap">
            <ul class="academy-content">
                <li>
                    <p class="no-opp">Questions, concerns or just fancy a chat? Please email <a href="mailto:careers@volumeglobal.com?subject=I'm interested in the Volume Academy">careers@volumeglobal.com</a> and our friendly HR team will get back to you within 24hours.</p>
                    <h2>Volume<span> Academy</span></h2>
                    <hr/>
                    <p>Are you reaching for that first rung of the career ladder? Today, it can seem increasingly
                        difficult to get a grip on it with a lack of experience – and so many candidates already half
                        way up.</p>

                    <p>But that needn’t been the case!</p>

                    <p>We provide a little lift that can make all the difference. And we call it The Volume Academy.</p>

                    <div class="video-holder">
                        <iframe width="100%" height="100%" src="https://www.youtube.com/embed/r6uu1NHJems?rel=0"
                                frameborder="0" allowfullscreen></iframe>
                    </div>
                    <p>We’re always looking to take on new talent, so if you’re interested in joining the Volume
                        Academy, simply email <a href="mailto:Volume.Academy@volumeglobal.com?subject=I'm interested in the Volume Academy"
                                                 class="hr-email">Volume.Academy@volumeglobal.com</a>. We can’t wait to hear from
                        you!</p>
                <li>
                    <div class="academy-logo">
                    </div>
                    <p>Our Academy allows students and recent graduates to work in a fast-paced digital environment with
                        some of the world’s biggest brands… and friendliest faces! During their time in the Academy,
                        members gain real-life, hands-on experience – whether that’s in our Client Services &
                        Support, Creative, Technology or Learning & Development team – and learn skills that will
                        prove invaluable during their job search, the early stages of their career and beyond.</p>

                    <p>Plus, many who have successfully completed placements have moved from our Academy into full-time
                        roles at Volume!</p>
                    <a href="mailto:Volume.Academy@volumeglobal.com?subject=I'm interested in the Volume Academy" class="cta">Apply
                        Now</a>
                </li>
            </ul>
        </div>
        <div class="clear"></div>
    </div>
</div>
    <?php get_footer(); ?>
</div><!--end paralax content-->
</div><!--end paralax holder-->
</div>
<div class="job-overlay" id="joblist-overlay">
    <div class="job-detail-wrap">
        <div class="jobdetail-head">
            <div class="icon"></div>
            <h3 class="titleOverlay"></h3>
            <div class="clear"></div>
        </div>
        <div class="forward-mobile">
            <a href="" class="forward-mail">FORWARD</a>
        </div>
        <div class="detail-wrap nano">
            <div class="nano-content">

                <div class="forward-section">
                    <a href="" class="forward-mail">FORWARD</a>
                </div>

                <div class="job-detail job-detail-holder">
                    <div class="job-detail-copy"></div>

                    <div class="desk-forward">
                        <a href="" class="forward-mail">FORWARD</a>
                    </div>
                </div>

                <div class="jobdetail-bottom job-detail-holder">
                    <div class="bottom-content uploadCV">

                        <form id="cvUploderForm" action="" method="post" name="test" enctype="multipart/form-data" class="drag-drop">
                            <h4>apply now</h4>
                            <h5 class="infoText"></h5>
                            <h5 class="infoTexSlug"></h5>
                            <div class="clear"></div>
                            <div class="message-block">
                                                 <span class="icon">
                                                 </span>
                                <label></label>
                            </div>
                            <div class="message-required">
                                <label>Following hightlighted fields are required</label>
                            </div>
                            <input class="jobid" type="hidden" name="jobid" value=""/>
                            <input class="c_slug" type="hidden" name="c_slug" value=""/>
                            <input type="hidden" name="data5" value=""/>
                            <ul>
                                <li><span class="special-label">Full Name:</span>
                                    <input class="input-name" type="text" name="name" placeholder="Your name" required></li>
                                <li><span class="special-label">Email:</span>
                                    <input type="email" name="email" placeholder="Your email address" required></li>
                                <li> <span class="special-label">Cover Letter:</span>
                                    <textarea name="cover" cols="30" rows="10" placeholder="A few words about yourself" required></textarea></li>
                            </ul>
                            <div class="notify-block">
                                <label></label>
                            </div>
                            <div class="progress-block">
                                <div class="loader"></div>
                            </div>
                            <div class="fileListHolder">
                                <ul class="fileList"></ul>
                            </div>
                            <div class="uploader drag-and-drop-zone">
                                <div class="browser">
                                    <label>
                                        <div class="drop-icon"></div>
                                        <div class="upload-title">DROP YOUR <strong>CV</strong> HERE</div>
                                        <div class="file-formats">(.doc, docx, pdf formats are allowed. You can attach only one document.)</div>
                                        <input type="file" name="cv_file" title="Click to add Files">
                                    </label>
                                </div>
                            </div>
                            <input type="submit" name="submit"  class="button-primary" value="submit" />
                        </form>
                    </div>
                    <div class="bottom-content share">
                        <h4>share now</h4>
                        <ul>
                            <li>
                                <a target="_blank" class="fb" title="Share"
                                   href="">

                                </a>
                            </li>
                            <li>
                                <a target="_blank" class="twitter" title="Tweet"
                                   href="">

                                </a>
                            </li>
                            <li>
                                <a target="_blank" class="linkedin" title="Share"
                                   href="">

                                </a>
                            </li>
<!--                            <li>-->
<!--                                <a target="_blank" class="mail" title="Share"-->
<!--                                   href="">-->
<!--                                </a>-->
<!--                            </li>-->
                        </ul>
                    </div>


                </div>
            </div>
        </div>
    </div>

</div>

<script>


    var jobList = <?= json_encode($jsArray) ?>;


    // variables for job upload
    var jobID = false;
    var slug = false;
    var attachments = true;
    var onProgress = false;
    var attachment = 0;
    $(".drag-and-drop-zone").dmUploader({
        url: '<?= THEMEROOT ?>/ajax-uploader.php',
        maxFileSize: 52428800,
        extraData: {
            'jobID': 'no need',
            'c_slug': 'no need',
            'attachments' : attachments
        },
        extFilter: 'pdf;doc;docx',
        //maxFiles: 1,
        onBeforeUpload: function (id) {
            console.log('Starting to upload #' + id);
        },
        onComplete: function () {
            console.log('We reach the end of the upload Queue!');
        },
        onUploadProgress: function (id, percent) {
            window.onProgress = true;
            //console.log(onProgress);
            console.log('Upload of #' + id + ' is at %' + percent);
            // do something cool here!
           // $('.fancybox-opened .uploadCV .message-block').removeClass('validation');
            //$('.fancybox-opened .uploadCV .message-block').removeClass('success');
            //$('.fancybox-opened .uploadCV .message-block').addClass('progress');
            //$('.fancybox-opened .uploadCV .message-block label').text('');
            //$('.fancybox-opened .uploadCV .progress-block').css({'display':'block'});
            //$('.fancybox-opened .uploadCV .progress-block').addClass('progress');
           //$('.fancybox-opened .uploadCV .progress-block label').text('Uploading CV.. ' + percent + '%');
            $('.fancybox-opened .uploadCV .progress-block').show();
            $('.fancybox-opened .uploadCV .progress-block .loader').width(percent+'%');
            $(".nano").nanoScroller({
                alwaysVisible: true
            });

        },
        onUploadSuccess: function (id, data) {
            onProgress = false;
            var data = JSON.parse(data);
            console.log(data);
            //$('.fancybox-opened .uploadCV .message-block').removeClass('progress');
            //$('.fancybox-opened .uploadCV .message-block').removeClass('validation');
            //$('.fancybox-opened .uploadCV .message-block label').text('');
            //$('.fancybox-opened .uploadCV .progress-block label').text('');
            //$('.fancybox-opened .uploadCV .progress-block').css({'display':'none'});
            //$(".nano").nanoScroller({ scroll: 'top' });
            $(".nano").nanoScroller({
                alwaysVisible: true
            });

            /**
             * Add new attachment to the attachments array
             */
            if(data.code == 202){
                $('.fancybox-opened .uploadCV .progress-block .loader').css({
                    //'background-color': '#95cf99'
                    'background-color': 'transparent'
                });
                $('.fancybox-opened .uploadCV .progress-block').hide();
                attachment = data.file;
                attachments = false;
                var holderElem = $('.fancybox-opened .uploadCV .fileListHolder .fileList');
                var fileElem = $('<li class="fileLinkHolder"><a title="View file" target="_blank" href="'+data.url+'">'+data.name+'</a></li>');
                var link = $('<span data="'+data.file+'" class="closeIcon" title="Remove file"><img src="<?= IMAGES ?>/file.png" width="50" height="auto"></span>');
                holderElem.append(fileElem).delay(10,function(){


                });
                fileElem.append(link);
                link.click(function(){
                    $('.fancybox-opened .uploadCV .notify-block')
                        .hide()
                        .children('label').html("");
                    $('.fancybox-opened .uploadCV .progress-block').show();
                    $('.fancybox-opened .uploadCV .progress-block .loader')
                        .width(0)
                        .css({
                            'background-color': '#95cf99'
                        });

                    $.ajax({
                        method: "POST",
                        url: '<?= THEMEROOT ?>/ajax-uploader.php',
                        data: {
                            action: 'delete',
                            data : {
                                id : $(this).attr('data')
                            }
                        }
                    })
                        .done(function( result ) {
                            var result = JSON.parse(result);
                            if(result.code == 200){

                                fileElem.remove();
                                attachments = true;
                                attachment = false;
                                $('.fancybox-opened .uploadCV .notify-block').hide();
                            }else{
                                console.log('sever error')
                            }
                        });

                });
                $('.fancybox-opened .uploadCV .notify-block').hide(function () {
                    $(".nano")
                        .stop()
                        .nanoScroller({ scrollTo: $('.fileListHolder')});
                });

            }else if(data.code == 406){
                //alert('please remove existing file')
                console.log(attachments);

                    //$('.fancybox-opened .uploadCV .progress-block').hide();
                //$(".nano").nanoScroller({ scrollTo: $('.notify-block')});
                $('.fancybox-opened .uploadCV .notify-block').show();
                $('.fancybox-opened .uploadCV .notify-block label').text('You can only upload one document. The following document is attached to your application.');


            }else{
                //alert('server error')
            }




        },
        onUploadError: function (id, message) {
            console.log('Error trying to upload #' + id + ': ' + message);
        },
//            onFileTypeError: function(file){
//                console.log('File type of ' + file.name + ' is not allowed: ' + file.type);
//            },
        onFileExtError: function (file) {
            //console.log('File extension of ' + file.name + ' is not allowed');
            //$('.fancybox-opened .uploadCV .message-block').removeClass('progress');
            //$('.fancybox-opened .uploadCV .message-block').removeClass('success');
            //$('.fancybox-opened .uploadCV .message-block label').text(' ');
            $('.fancybox-opened .uploadCV .notify-block').show();
            $('.fancybox-opened .uploadCV .notify-block').addClass('validation');
            $('.fancybox-opened .uploadCV .notify-block label').text('');
            $('.fancybox-opened .uploadCV .notify-block label').text('File extension of ' + file.name + ' is not allowed. Only .doc, .docx, .pdf formats are allowed.');
            /*$(".nano").nanoScroller({
                alwaysVisible: true
            });*/

        },
        onFilesMaxError: function (file) {
            $('.fancybox-opened .uploadCV .message-block').removeClass('progress');
            $('.fancybox-opened .uploadCV .message-block').removeClass('validation');
            $('.fancybox-opened .uploadCV .message-block label').text(' ');
            $('.fancybox-opened .uploadCV .message-block').addClass('success');
            $('.fancybox-opened .uploadCV .message-block label').text('You have already applied to this position.');
            console.log('You have already applied to this position.');
            //$(".nano").nanoScroller({ scroll: 'top' });
        }
    });

    var options = {
        beforeSubmit:  showRequest,  // pre-submit callback
        success:       showResponse,  // post-submit callback
        data: {
            action: 'submit',
            data : {
                job : jobID,
                slug : slug,
                attachment : attachment
            }
        },
        url: '<?= THEMEROOT ?>/ajax-uploader.php',
        dataType:  'json',        // 'xml', 'script', or 'json' (expected server response type)
        clearForm: true,        // clear all form fields after successful submit
        resetForm: true      // reset the form after successful submit
    };


    /**
     * Validation
     */
    $("#cvUploderForm").validate({
        submitHandler: function(form) {
            console.log(onProgress);
            if(!attachment && onProgress){
                //alert('please upload ur cv');
                $('.fancybox-opened .uploadCV .notify-block').show();
                $('.fancybox-opened .uploadCV .notify-block').addClass('validation');
                $('.fancybox-opened .uploadCV .notify-block label').text('');
                $('.fancybox-opened .uploadCV .notify-block label').text('Please upload your CV.');
                return false;
            }else{
                console.log('cvUploderForm clicked')
                $(form).ajaxSubmit(options)
            }

        },
        invalidHandler : function () {
            //$('.job-overlay .bottom-content.uploadCV .message-required').show();
            //$(".nano").nanoScroller({ scroll: 'top' });
        },
        messages: {
            name: "This is a required field",
            cover: "This is a required field",
            email: {
                required: "This is a required field",
            }
        }
    });


    /**
     * Trigger before submit ajax call
     * @param formData
     * @param jqForm
     * @param options
     * @returns {boolean}
     */
    function showRequest(formData, jqForm, options) {
        jQuery( "#cvUploderForm .button-primary" ).prop( "disabled", true);
        window.onProgress = true;
        // formData is an array; here we use $.param to convert it to a string to display it
        // but the form plugin does this for you automatically when it submits the data
        var queryString = $.param(formData);

        // jqForm is a jQuery object encapsulating the form element.  To access the
        // DOM element for the form do this:
        // var formElement = jqForm[0];

        //alert('About to submit: \n\n' + queryString);
        return true;
    }

    /**
     * Ajax request callback
     * @param responseText
     * @param statusText
     * @param xhr
     * @param $form
     */
    function showResponse(responseText, statusText, xhr, $form)  {
        window.onProgress = false;
        attachments = true;
        attachment = false;
        var holderElem = $('.fancybox-opened .uploadCV .fileListHolder .fileList');
        holderElem.html("");
        //alert('status: ' + statusText + '\n\nresponseText: \n' + JSON.stringify(responseText) +
            //'\n\nThe output div should have already been updated with the responseText.');
        $('.fancybox-opened .uploadCV .notify-block').hide();
        $('.fancybox-opened .uploadCV .message-block').show();
        $('.fancybox-opened .uploadCV .message-block').addClass('success');
        $('.fancybox-opened .uploadCV .message-block label').text('Thank you for your CV, we will review and contact you as soon as we can.');
        $(".nano").nanoScroller({ scroll: 'top' });
        jQuery( "#cvUploderForm .button-primary" ).prop( "disabled", false);
    }

    $(document).ready(function(){
        //add a class for IE specific versions
        if (/MSIE (\d+\.\d+);/.test(navigator.userAgent))
        {
            var ieversion=new Number(RegExp.$1);
            if (ieversion == 9) {
                $('body').addClass("ie9");
            }
        }
    });

    $('.fancyBoxSmallPrint').fancybox();
</script>


