<?php
/**
 * The template for displaying the footer
 *
 *
 * @link
 * @since 1.0
 *
 * @package WordPress
 * @subpackage Volume People
 * @author Kashif
 */
?>


<div class="footer full-width" id="10">
<div class="wrap">
    <ul class="footer-top">
        <li class="tweet content-column">
            <h5>Latest tweet</h5>


<!--            <p> --><?//= display_latest_tweets('andrew_biggart'); ?><!-- </p>-->

            <p><?php

    /* your parameters */
    $jltw_args = array(
        'username'  => 'VolumeLtd',
        'nb_tweets' => 1,
        'avatar'    => false,
        'cache'     => 1600,
        'transition'    => true,
        'delay'     => 10,
        'links'     => false
    );

    /* set variable */

    $list_of_tweets = get_jltw($jltw_args);

    /* more later */
    echo $list_of_tweets;?> </p>
        </li>
        <li class="social mobile-hide content-column">
            <h5>Social</h5>
            <ul>
                <li><a href="https://www.facebook.com/VolumeLtd" target="_blank" class="fb"></a></li>
                <li><a href="https://twitter.com/VolumeLtd" target="_blank" class="twitter"></a></li>
                <li><a href="https://www.youtube.com/user/VolumeGroup" target="_blank" class="you-tube"></a></li>
                <li><a href="https://www.linkedin.com/company/volume" target="_blank" class="linkedin"></a></li>
                <!--<li><a href="#" class="pintrest"></a></li>-->
                <li><a href="https://www.behance.net/VolumeLtd" target="_blank" class="behance"></a></li>
            </ul>
        </li>
        <li class="content-column"><h5>Telephone</h5>
            <p>+44 (0)118 977 5800</p>
            <h5>Address</h5>
            <p class="mobile-hide">Buckhurst Court<br>
                London Road<br>
                Wokingham<br>
                Berkshire<br>
                RG40 1PA</p>
            <p class="only-potrait">Buckhurst Court, London Road <br>Wokingham, Berkshire<br> RG40 1PA</p>
        </li>
        <li class="content-column">
            <h5>email</h5>
            <p><a href="mailto:careers@volumeglobal.com">careers@volumeglobal.com</a></p>
            <h5>Web</h5>
            <p><a href="http://www.volumeglobal.com" target="_blank">www.volumeglobal.com</a><br>
            <a href="http://www.volume.lk" target="_blank">www.volume.lk</a></p>
        </li>
        <li class="social content-column only-potrait">
            <ul>
                <li><a href="https://www.facebook.com/VolumeLtd" target="_blank" class="fb"></a></li>
                <li><a href="https://twitter.com/VolumeLtd" target="_blank" class="twitter"></a></li>
                <li><a href="https://www.youtube.com/user/VolumeGroup" target="_blank" class="you-tube"></a></li>
                <li><a href="https://www.linkedin.com/company/volume" target="_blank" class="linkedin"></a></li>
                <!--<li><a href="#" class="pintrest"></a></li>-->
                <li><a href="https://www.behance.net/VolumeLtd" target="_blank" class="behance"></a></li>
            </ul>
        </li>
    </ul>
    <div class="clear"></div>
    <div class="footer-bottom">
        <ul class="left">
            <li><p>&copy; Volume Ltd. <?= date('Y') ?> </p></li>
            <li><a href="https://www.volumeglobal.com/privacy" target="_blank">Privacy</a></li>
            <li><a href="https://www.volumeglobal.com/terms-and-conditions" target="_blank">Terms & Conditions</a></li>
            <li><a href="https://www.volumeglobal.com/cookies" target="_blank">How we use cookies</a></li>
            <li><a href="https://www.volumeglobal.com/leadership" target="_blank">Leadership</a></li>
        </ul>
        <ul class="right">
            <li><a class="digital" href="http://bit.ly/1MorjhQ" target="_blank"></a></li>
            <li><a class="wirehive" href="http://bit.ly/1KbRra0" target="_blank"></a></li>
            <li><a class="be" href="http://bit.ly/1Qe7RSW" target="_blank"></a></li>
            <li><a class="bea" href="http://bit.ly/1US1fLb" target="_blank"></a></li>
            <li><a class="thames" href="http://bit.ly/1J9eK2t" target="_blank"></a></li>
        </ul>
    </div>
    <div class="clear"></div>
</div>

</div>
<?php wp_footer(); ?> 
</body>
</html>
