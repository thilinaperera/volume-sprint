<?php
/**
 * Template Name: Home
 *
 * @link
 * @since 1.0
 *
 * @package WordPress
 * @subpackage Volume People
 * @author Kashif
 */
get_header(); ?>
<script>
    cookeiHandler.checkCookie();
</script>
<link rel="stylesheet" type="text/css" href="<?= get_stylesheet_directory_uri() ?>/styles/home-style.css">
<div id="front-page">
    <div id="fsvs-body">
        <div class="slide nth-class-1">
            <video autoplay loop poster="<?php echo get_template_directory_uri(); ?>/images/slide-1.jpg" id="bgvid">

                <source src="<?php echo get_template_directory_uri(); ?>/banner003.webm" type="video/webm">
                <source src="<?php echo get_template_directory_uri(); ?>/banner003.mp4" type="video/mp4">
            </video>
            <div class="copy-area">
                <h1>Welcome</h1>
                <p>We're a truly exciting, forward-thinking agency...<br>bursting with energetic, fun-loving people.</p>
            </div>
            <a class="arrow down" id="1">
                <div class="arrow-hover"></div>
            </a>
            <!--        <a href="--><?php //bloginfo('url'); ?><!--/job" class="career-badge"></a>-->
        </div>
    <div class="slide nth-class-2">
        <div class="copy-area">
            <h2>Fun-Loving <span>People</span></h2>
            <p>Our unique, high-energy culture is shaped by incredibly smart and passionate individuals from all walks of life.</p>
            <a href="<?php bloginfo('url'); ?>/people">Tell me more</a>
        </div>
        <a class="arrow up" id="0">
            <div class="arrow-hover"></div>
        </a>
        <a class="arrow down" id="2">
            <div class="arrow-hover"></div>
        </a>
<!--        <a href="--><?php //bloginfo('url'); ?><!--/job" class="career-badge"></a>-->
    </div>
    <div class="slide nth-class-3">
        <div class="copy-area">
            <h2>Forward-Thinking <span>Company</span></h2>
            <p>We’ve created a dynamic work environment that encourages entrepreneurial thinking and collaborative innovation.</p>
            <a href="<?php bloginfo('url'); ?>/company">Tell me more</a>
        </div>
        <a class="arrow up" id="1">
            <div class="arrow-hover"></div>
        </a>
        <a class="arrow down" id="3">
            <div class="arrow-hover"></div>
        </a>
<!--        <a href="--><?php //bloginfo('url'); ?><!--/job"class="career-badge"></a>-->
    </div>
    <div class="slide nth-class-4">
        <div class="copy-area">
            <h2>WIDE-RANGING <span>BENEFITS</span></h2>
            <p>We invest in the wellbeing of our employees to ensure they remain happy, healthy and productive.</p>
            <a href="<?php bloginfo('url'); ?>/benefits">Tell me more</a>
        </div>
        <a class="arrow up" id="2">
            <div class="arrow-hover"></div>
        </a>
        <a class="arrow down" id="4">
            <div class="arrow-hover"></div>
        </a>
<!--        <a href="--><?php //bloginfo('url'); ?><!--/job" class="career-badge"></a>-->
    </div>
    <div class="slide nth-class-5">

        <div class="copy-area">
            <h2>Career-Boosting<span> Opportunities</span></h2>
            <p>Join our journey and work with some of the world’s biggest and best brands.</p>
            <a href="<?php bloginfo('url'); ?>/job">Tell me more</a>
        </div>
        <a class="arrow up" id="3">
            <div class="arrow-hover"></div>
        </a>
    </div>
</div>
<ul id="fsvs-pagination" style="margin-top: -53px; right: 25px;">
    <li class="pagination-link"><span style="transition: all 0.2s ease 0s;"><span style="transition: all 0.2s ease 0s;"></span></span></li>
    <li class="pagination-link active"><span style="transition: all 1s ease 0s;"><span style="transition: all 0.2s ease 0s;"></span></span></li>
    <li class="pagination-link"><span style="transition: all 1s ease 0s;"><span style="transition: all 0.2s ease 0s;"></span></span></li>
    <li class="pagination-link"><span style="transition: all 1s ease 0s;"><span style="transition: all 0.2s ease 0s;"></span></span></li></ul>
    <div class="loading-panel">
        <div class="text-middle">
            <div class="logo">
            </div>
            <h2>well <span>hello there</span></h2>
        </div>
    </div>
</div>

<div class="front-in-mobile">
    <a href="<?php bloginfo('url') ?>" class="logo">&nbsp;</a>

    <div class="slot-people">
        <a href="<?php bloginfo('url'); ?>/people"></a>
        <img src="<?php echo get_template_directory_uri(); ?>/images/img1.jpg" />
        <aside class="copy-area">
            <h2>Fun-Loving <span>People</span></h2>
            <label>Tell me more</label>
        </aside>
    </div>
    <div class="slot-company">
        <a href="<?php bloginfo('url'); ?>/company"></a>
        <img src="<?php echo get_template_directory_uri(); ?>/images/img2.jpg" />
        <aside class="copy-area">
            <h2>Forward-Thinking <span>Company</span></h2>
            <label>Tell me more</label>
        </aside>
    </div>
    <div class="slot-benefits">
        <a href="<?php bloginfo('url'); ?>/benefits"></a>
        <img src="<?php echo get_template_directory_uri(); ?>/images/img3.jpg" />
        <aside class="copy-area">
            <h2>WIDE-RANGING <span>BENEFITS</span></h2>
            <label>Tell me more</label>
        </aside>
    </div>
    <div class="slot-opportunities">
        <a href="<?php bloginfo('url'); ?>/job"></a>
        <img src="<?php echo get_template_directory_uri(); ?>/images/img4.jpg" />
        <aside class="copy-area">
            <h2>Career-Boosting<span> Opportunities</span></h2>
            <label>Tell me more</label>
        </aside>
    </div>
</div>

</body>
</html>

<script>
    var slider;
    function sliderInit(){
        slider = $.fn.fsvs({
            speed : 1000,
            bodyID : 'fsvs-body',
            selector : '> .slide',
            mouseSwipeDisance : 20,
            afterSlide : function(){
                //if ($('.home-header .header-inner.nav-active').length < 1) {
                if ($('.nth-class-3.active-slide').length > 0 || $('.nth-class-5.active-slide').length > 0) {
                    $('.home-header .header-inner .wrap').addClass('third-slide');
                } else {
                    $('.home-header .header-inner .wrap').removeClass('third-slide');
                }
                //}

            },
            beforeSlide : function(){

                if ($('.nth-class-3.active-slide').length > 0 || $('.nth-class-5.active-slide').length > 0) {
                    $('#fsvs-pagination  li  span').css({'background-color': '#000000'});
                } else {
                    $('#fsvs-pagination  li  span').css({'background-color': '#ffffff'});
                }

                closeNavigationSlides ();
            },
            endSlide : function(){},
            mouseWheelEvents : true,
            mouseWheelDelay : false,
            scrollabelArea : 'scrollable',
            mouseDragEvents : true,
            touchEvents : true,
            arrowKeyEvents : true,
            pagination : true,
            nthClasses : false,
            detectHash : false
        });
        $('#fsvs-pagination, #fsvs-body, .home-header.landing-page').show();
    }
    $(document).ready( function() {
        $('.home-header.landing-page').hide();
        middleLoading();


        //slider.slideUp();
        //slider.slideDown();
        //slider.slideToIndex( index );
       //slider.unbind();
        //slider.rebind();

        $('.slide .arrow').each(function(){
            $(this).bind( 'click touchstart',function(){
                var slideID = $(this).attr('id');
                slider.slideToIndex(slideID);
                if($('.nth-class-3.active-slide').length > 0 || $('.nth-class-5.active-slide').length > 0) {
                    $('#fsvs-pagination  li  span').css({'background-color':'#000000'});
                } else {
                    $('#fsvs-pagination  li  span').css({'background-color':'#ffffff'});
                }
            });
        });

        $('.home-header .logo').click(function(e){
            e.preventDefault();
            slider.slideToIndex(0);
        });

    });

    $(window).load(function(){
        //var isLoading = getParameterByName('no-loading');
        console.log(returnedUser);
        if( !returnedUser){
            console.log('fals2 & view');
            $('.loading-panel').show();
            $('#fsvs-body').css({'opacity':'1'});
            $('.loading-panel').delay(3000).fadeOut(function(){
                sliderInit();
                openNavigationSlides ();
            });
        }else {
            $('#fsvs-body').css({'opacity':'1'});
            sliderInit();
            openNavigationSlides ();

        }


        //slider.rebind();
    })

    function getParameterByName(name) {
        name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
        var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
            results = regex.exec(location.search);
        return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
    }

    /*function openNavigation (){
        $('.home-header ul').slideDown();
        $('.home-header .header-inner').addClass('nav-active open');
    }*/

    function openNavigationSlides (){
        $('.home-header ul').slideDown();
        $('.home-header .header-inner').addClass('nav-active open');
    }

    function closeNavigationSlides (){
        $('.home-header ul').slideUp();
        $('.home-header .header-inner').removeClass('nav-active open');
    }

    function middleLoading () {
        var parent = $('.loading-panel');
        var parentDimensions = parent.height() * parent.width();
        var child = $('.loading-panel .text-middle');
        var childDimensions = child.height() * child.width();
        child.css({ top: (parent.height() / 2 - child.height() / 2)-150, left: (parent.width() / 2 - child.width() / 2)- 210 });

        if($(window).width() < 1030) {
            child.css({ top: (parent.height() / 2 - child.height() / 2)-150, left: (parent.width() / 2 - child.width() / 2)- 190 });
        } else {
            child.css({ top: (parent.height() / 2 - child.height() / 2)-150, left: (parent.width() / 2 - child.width() / 2)- 220 });
        }
    }

</script>
