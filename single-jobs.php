<?php
/**
 * Template Name: Single Job
 *
 * The template for displaying all the single jobs
 *
 *
 * @link
 * @since 1.0
 *
 * @package WordPress
 * @subpackage Volume People
 * @author Kashif
 */
get_header(); ?>


<?php



$args = array('post_type' => 'jobman_job', 'posts_per_page' => -1 );
$the_query = new WP_Query( $args );


?>



<?php if ( have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post(); ?>



    <div>
        <a href="<?php the_permalink(); ?>"> <h1> <?php the_title(); ?> </h1> </a>

    </div>


<?php endwhile; endif; wp_reset_Query(); ?>




<?php get_footer(); ?>

