/**
 * Created by thilina.perera on 5/26/2016.
 */
// variables for job upload
var jobID = false;
var slug = false;
var attachments = true;
var onProgress = false;
var fileCheck = false;
var attachment = 0;
var options = {
    beforeSubmit:  showRequest,  // pre-submit callback
    success:  showResponse,  // post-submit callback
    data: {
        action: 'submit',
        data : {
            job : jobID,
            slug : slug,
            attachment : attachment
        }
    },
    url: ajaxPath.ajaxurl,
    dataType:  'json',        // 'xml', 'script', or 'json' (expected server response type)
    clearForm: true,        // clear all form fields after successful submit
    resetForm: true      // reset the form after successful submit
};
/**
 * Trigger before submit ajax call
 * @param formData
 * @param jqForm
 * @param options
 * @returns {boolean}
 */
function showRequest(formData, jqForm, options) {
    console.log('showRequest')
    jQuery( "#cvUploderForm .button-primary" )
        .prop( "disabled", true)
        .addClass('submitting');
    if(jQuery( "#cvUploderForm .button-primary").hasClass('submitting')){
        jQuery( "#cvUploderForm .submit-section .primary").show();
        setTimeout(function() {
            if(jQuery( "#cvUploderForm .button-primary").hasClass('submitting')) {
                showResponse();
            }
        }, 10000);
    }

    window.onProgress = true;
    // formData is an array; here we use $.param to convert it to a string to display it
    // but the form plugin does this for you automatically when it submits the data
    var queryString = $.param(formData);

    // jqForm is a jQuery object encapsulating the form element.  To access the
    // DOM element for the form do this:
    // var formElement = jqForm[0];

    //alert('About to submit: \n\n' + queryString);
    return true;
}

/**
 * Ajax request callback
 * @param responseText
 * @param statusText
 * @param xhr
 * @param $form
 */
function showResponse(responseText, statusText, xhr, $form)  {
    window.onProgress = false;
    attachments = true;
    attachment = false;
    var holderElem = $('.fancybox-opened .uploadCV .fileListHolder .fileList');
    holderElem.html("");
    //alert('status: ' + statusText + '\n\nresponseText: \n' + JSON.stringify(responseText) +
    //'\n\nThe output div should have already been updated with the responseText.');
    $('.fancybox-opened .uploadCV .notify-block').hide();
    $('.fancybox-opened .uploadCV .message-block').show();
    $('.fancybox-opened .uploadCV .message-block').addClass('success');
    $('.fancybox-opened .uploadCV .message-block label').text('Thank you for your CV, we will review and contact you as soon as we can.');
    $(".nano").nanoScroller({ scroll: 'top' });
    jQuery( "#cvUploderForm .button-primary" )
        .prop( "disabled", false)
        .removeClass('submitting');
    jQuery( "#cvUploderForm .submit-section p").hide();
}

jQuery(document).ready(function () {

    if(!jQuery('body').hasClass('body-jobs-page')){
        return true;
    }
    console.log('body-jobs-page');
    $(".drag-and-drop-zone").dmUploader({
        url: ajaxPath.ajaxurl,
        maxFileSize: 52428800,
        extraData: {
            'jobID': 'no need',
            'c_slug': 'no need',
            'attachments' : attachments
        },
        extFilter: 'pdf;doc;docx',
        //maxFiles: 1,
        onBeforeUpload: function (id) {
            console.log('Starting to upload #' + id);
        },
        onComplete: function () {
            console.log('We reach the end of the upload Queue!');
        },
        onUploadProgress: function (id, percent) {
            window.onProgress = true;
            //console.log(onProgress);
            console.log('Upload of #' + id + ' is at %' + percent);
            // do something cool here!
            // $('.fancybox-opened .uploadCV .message-block').removeClass('validation');
            //$('.fancybox-opened .uploadCV .message-block').removeClass('success');
            //$('.fancybox-opened .uploadCV .message-block').addClass('progress');
            //$('.fancybox-opened .uploadCV .message-block label').text('');
            //$('.fancybox-opened .uploadCV .progress-block').css({'display':'block'});
            //$('.fancybox-opened .uploadCV .progress-block').addClass('progress');
            //$('.fancybox-opened .uploadCV .progress-block label').text('Uploading CV.. ' + percent + '%');
            $('.fancybox-opened .uploadCV .progress-block').show();
            $('.fancybox-opened .uploadCV .progress-block .loader').width(percent+'%');
            $(".nano").nanoScroller({
                alwaysVisible: true
            });

        },
        onUploadSuccess: function (id, data) {
            onProgress = false;
            var data = JSON.parse(data);
            console.log(data);
            //$('.fancybox-opened .uploadCV .message-block').removeClass('progress');
            //$('.fancybox-opened .uploadCV .message-block').removeClass('validation');
            //$('.fancybox-opened .uploadCV .message-block label').text('');
            //$('.fancybox-opened .uploadCV .progress-block label').text('');
            //$('.fancybox-opened .uploadCV .progress-block').css({'display':'none'});
            //$(".nano").nanoScroller({ scroll: 'top' });
            $(".nano").nanoScroller({
                alwaysVisible: true
            });

            /**
             * Add new attachment to the attachments array
             */
            if(data.code == 202){
                $('.fancybox-opened .uploadCV .progress-block .loader').css({
                    //'background-color': '#95cf99'
                    'background-color': 'transparent'
                });
                $('.fancybox-opened .uploadCV .progress-block').hide();
                attachment = data.file;
                attachments = false;
                /**
                 * set file check to true
                 * @type {jQuery|HTMLElement}
                 */
                fileCheck = true;

                var holderElem = $('.fancybox-opened .uploadCV .fileListHolder .fileList');
                var fileElem = $('<li class="fileLinkHolder"><a title="View file" target="_blank" href="'+data.url+'">'+data.name+'</a></li>');
                var link = $('<span data="'+data.file+'" class="closeIcon" title="Remove file"><img src="'+ajaxPath.themeroot+'/images/file.png" width="50" height="auto"></span>');
                holderElem.append(fileElem).delay(10,function(){


                });
                fileElem.append(link);
                link.click(function(){

                    /**
                     * reset file checker
                     */
                    fileCheck = false;

                    $('.fancybox-opened .uploadCV .notify-block')
                        .hide()
                        .children('label').html("");
                    $('.fancybox-opened .uploadCV .progress-block').show();
                    $('.fancybox-opened .uploadCV .progress-block .loader')
                        .width(0)
                        .css({
                            'background-color': '#95cf99'
                        });

                    $.ajax({
                            method: "POST",
                            url: ajaxPath.ajaxurl,
                            data: {
                                action: 'delete',
                                data : {
                                    id : $(this).attr('data')
                                }
                            }
                        })
                        .done(function( result ) {
                            var result = JSON.parse(result);
                            if(result.code == 200){

                                fileElem.remove();
                                attachments = true;
                                attachment = false;
                                $('.fancybox-opened .uploadCV .notify-block').hide();
                            }else{
                                console.log('sever error')
                            }
                        });

                });
                $('.fancybox-opened .uploadCV .notify-block').hide(function () {
                    $(".nano")
                        .stop()
                        .nanoScroller({ scrollTo: $('.fileListHolder')});
                });

            }else if(data.code == 406){
                //alert('please remove existing file')
                console.log(attachments);

                //$('.fancybox-opened .uploadCV .progress-block').hide();
                //$(".nano").nanoScroller({ scrollTo: $('.notify-block')});
                $('.fancybox-opened .uploadCV .notify-block').show();
                $('.fancybox-opened .uploadCV .notify-block label').text('You can only upload one document. The following document is attached to your application.');


            }else{
                //alert('server error')
            }




        },
        onUploadError: function (id, message) {
            console.log('Error trying to upload #' + id + ': ' + message);
        },
//            onFileTypeError: function(file){
//                console.log('File type of ' + file.name + ' is not allowed: ' + file.type);
//            },
        onFileExtError: function (file) {
            //console.log('File extension of ' + file.name + ' is not allowed');
            //$('.fancybox-opened .uploadCV .message-block').removeClass('progress');
            //$('.fancybox-opened .uploadCV .message-block').removeClass('success');
            //$('.fancybox-opened .uploadCV .message-block label').text(' ');
            $('.fancybox-opened .uploadCV .notify-block').show();
            $('.fancybox-opened .uploadCV .notify-block').addClass('validation');
            $('.fancybox-opened .uploadCV .notify-block label').text('');
            $('.fancybox-opened .uploadCV .notify-block label').text('File extension of ' + file.name + ' is not allowed. Only .doc, .docx, .pdf formats are allowed.');
            /*$(".nano").nanoScroller({
             alwaysVisible: true
             });*/

        },
        onFilesMaxError: function (file) {
            $('.fancybox-opened .uploadCV .message-block').removeClass('progress');
            $('.fancybox-opened .uploadCV .message-block').removeClass('validation');
            $('.fancybox-opened .uploadCV .message-block label').text(' ');
            $('.fancybox-opened .uploadCV .message-block').addClass('success');
            $('.fancybox-opened .uploadCV .message-block label').text('You have already applied to this position.');
            console.log('You have already applied to this position.');
            //$(".nano").nanoScroller({ scroll: 'top' });
        }
    });

    /**
     * Validation
     */
    $("#cvUploderForm").validate({
        submitHandler: function(form) {
            console.log(onProgress);
            if(!attachment && onProgress || !fileCheck){
                //alert('please upload ur cv');
                $('.fancybox-opened .uploadCV .notify-block').show();
                $('.fancybox-opened .uploadCV .notify-block').addClass('validation');
                $('.fancybox-opened .uploadCV .notify-block label').text('');
                $('.fancybox-opened .uploadCV .notify-block label').text('Please upload your CV.');
                return false;
            }else{
                $(form).ajaxSubmit(options);
                /**
                 * reset file checker
                 */
                fileCheck = false;
            }

        },
        invalidHandler : function () {
            //$('.job-overlay .bottom-content.uploadCV .message-required').show();
            //$(".nano").nanoScroller({ scroll: 'top' });
        },
        messages: {
            name: "This is a required field",
            cover: "This is a required field",
            email: {
                required: "This is a required field",
            }
        }
    });
    $('.fancyBoxSmallPrint').fancybox();

});











