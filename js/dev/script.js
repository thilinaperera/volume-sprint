/**
 * @ToDo : Mokshi: remove this
 * if(((wWidth) < 740 )&& !(jQuery('body').hasClass('device-landscape'))&& !(jQuery('body').hasClass('iphone'))){
     jQuery('body').addClass("phone-landscape");
 * }
 */
/**
 * Do not remove below lines - Thilina
 */
var global = {
    init : function () {
        var $this = this;
        this.bind(document,'ready',function(){
            $this.helpers.listBoxGetFirstBox();
        });
        this.bind(window,'resize',$this.helpers.listBoxHeightFixReload);
        this.bind(document,'reday')
    },
    helpers : {
        fancyBoxSpace : 100,
        listBoxGetFirstBox : function(rtrn){
            var regionSelect;
            var regionSelectElem;

            jQuery('.location-holder').each(function(){
                if(jQuery(this).parent().hasClass('selected')){
                    regionSelect = jQuery(this).attr('data-region-id');
                    regionSelectElem = jQuery('.featured-job'+regionSelect)
                    if(typeof rtrn == "undefined"){
                        global.helpers.listBoxHeightFix( regionSelectElem );
                    }
                }
            });
            if(typeof rtrn != "undefined"){
                return regionSelectElem;
            }
        },
        listBoxHeightFix : function(){
            if( !jQuery('.careers-main .job-list .list-holder li').hasClass("mobile") ){
                return true;
            }
            var $elem = arguments[0];
            var biggestHeaderHeight = 0;


            /**
             * clear all
             */
            $elem.find('.job-title-header, .job-summary').css({
                height : ''
            });


            $elem.find('.job-title-header').each(function(){
                if($(this).height() > biggestHeaderHeight){
                    biggestHeaderHeight = $(this).height();
                }
            });
            $elem.find('.job-title-header').height(biggestHeaderHeight);

            /**
             * p height fix
             */
            var biggestPHeight = 0;
            $elem.find('.job-summary').each(function(){
                if($(this).height() > biggestPHeight){
                    biggestPHeight = $(this).height();
                }
            });
            $elem.find('.job-summary').height(biggestPHeight);

        },
        listBoxHeightFixReload : function(){
            var $elem = global.helpers.listBoxGetFirstBox(true);
            console.log($elem);
            console.log('listBoxHeightFixReload');
            if( !jQuery('.careers-main .job-list .list-holder li').hasClass("mobile") ){
                return true;
            }
            console.log('has mobile');

            $elem.find('.job-title-header, .job-summary').css({
                height : ''
            });
            global.helpers.listBoxGetFirstBox();
        }
    },
    bind : function ($elem,$event,$func){
        jQuery($elem).on($event,$func);
    },
};
global.init();



jQuery(document).ready(function () {
    enquire.register("screen and (max-width: 740px) and (orientation: landscape)", {

        match : function() {
            console.log(" < 740 L");
            console.log(jQuery);
            jQuery('body').addClass("phone-landscape");

            jQuery(".vacant-show").cycle({
                speed: 100,
                timeout: 0,
                slides: '.vacant-content',
                next: '#next',
                prev: '#prev',
                pager: '#pager',
                pagerActiveClass: 'pager',
                pagerTemplate: '',
                fx: 'slide',
                log: false,
                pauseOnHover: true,
                //swipe:true
            });

            $('.page-button').click(function(){
                if($('#Wokingham').hasClass('cycle-slide-active')){
                    var slideHeight = $('#Wokingham').height() + 20;
                    $('.vacant-show').height(slideHeight);
                }

                if($('#Plymouth').hasClass('cycle-slide-active')){
                    var slideHeight = $('#Plymouth').height() + 20;
                    $('.vacant-show').height(slideHeight);
                }

                if($('#Colombo').hasClass('cycle-slide-active')){
                    var slideHeight = $('#Colombo').height() + 20;
                    $('.vacant-show').height(slideHeight);
                }

            });
        },

        unmatch : function() {
            console.log(" > 740 | P");
            jQuery('body').removeClass("phone-landscape");
        },

    });

    enquire.register("screen and (max-width: 666px)", {

        match : function() {
            global.helpers.fancyBoxSpace = 0;
        },

        unmatch : function() {
            global.helpers.fancyBoxSpace = 100;
        },

    });

});





var cookieName = "home";
var returnedUser = false;
var cookeiHandler = {
    writeCookie : function (name,value,days) {
        var domain, domainParts, date, expires, host;

        if (days)
        {
            date = new Date();
            date.setTime(date.getTime()+(days*24*60*60*1000));
            expires = "; expires="+date.toGMTString();
        }
        else
        {
            expires = "";
        }

        host = location.host;
        if (host.split('.').length === 1)
        {
            // no "." in a domain - it's localhost or something similar
            document.cookie = name+"="+value+expires+"; path=/";
        }
        else
        {
            domainParts = host.split('.');
            domainParts.shift();
            domain = '.'+domainParts.join('.');

            document.cookie = name+"="+value+expires+"; path=/; domain="+domain;
            if (cookeiHandler.get(name) == null || cookeiHandler.get(name) != value)
            {
                domain = '.'+host;

                document.cookie = name+"="+value+expires+"; path=/; domain="+domain;
            }
        }


    },
    checkCookie : function () {
        if(cookeiHandler.readCookie(cookieName) == ""){
            cookeiHandler.setCookie();
            returnedUser = false;
        }else{
            var homeCookie = JSON.parse(decodeURIComponent(cookeiHandler.readCookie(cookieName)));
            if(homeCookie.status == true){
                returnedUser = true;
            }
        }
    },
    readCookie : function(name) {
        var i, c, ca, nameEQ = name + "=";
        ca = document.cookie.split(';');
        for(i=0;i < ca.length;i++) {
            c = ca[i];
            while (c.charAt(0)==' ') {
                c = c.substring(1,c.length);
            }
            if (c.indexOf(nameEQ) == 0) {
                return c.substring(nameEQ.length,c.length);
            }
        }
        return '';
    },
    get: function(name){
        var nameEQ = name + "=";
        var ca = document.cookie.split(';');
        for (var i=0; i < ca.length; i++)
        {
            var c = ca[i];
            while (c.charAt(0)==' ')
            {
                c = c.substring(1,c.length);
            }

            if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
        }
        return null;
    },
    setCookie : function () {
        var setData = {
            status : true
        };
        setData = JSON.stringify(setData);
        cookeiHandler.writeCookie(cookieName,encodeURIComponent(setData));
    },
    cookieJson : function () {
        return JSON.parse(decodeURIComponent(cookeiHandler.readCookie(cookieName)));
    }
}


/**
 *
 * waypoint and others
 */

var waypointObj = [];
var waypointObjBenefit = [];
var waypointObjCareers = [];
var waypointObjFooter = [];
var waypointObjNavigation =  [];
jQuery(document).ready(function(){
    extraLogo();
    //jQuery('.job-overlay .bottom-content.uploadCV form').find(':input[placeholder]').placeholder();
    jQuery('.job-overlay .bottom-content.uploadCV form input[placeholder]').placeholder();
   // jQuery(".nano").nanoScroller({
   //     alwaysVisible: true
    //});
    jQuery('.nav-cta').click(function(){
        if(jQuery(this).parent().parent().hasClass('open')){
            closeNavigation ();
        }else {
            openNavigation ();
        }
    });
    var wWidth = jQuery(window).width();

    //Jobs page slider activate
    //jQuery(".vacant-show").cycle({
    //    speed: 100,
    //    timeout: 0,
    //    slides: '.vacant-content',
    //    next: '#next',
    //    prev: '#prev',
    //    pager: '#pager',
    //    pagerActiveClass: 'pager',
    //    pagerTemplate: '',
    //    fx: 'slide',
    //    log: false,
    //    pauseOnHover: true,
    //    //swipe:true
    //});

    //
    if(!(/Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent)) && (wWidth) < 756 ) {
        jQuery('body').addClass("responsive-small");
    }

    if((wWidth) < 666 ) {
        jQuery('.fancybox-close').attr('style','right: 15px');
        jQuery('.job-overlay .job-detail .job-detail-copy').attr('style','padding-left: 10px');
    }
    else {
       // jQuery('.fancybox-close').attr('style','right: 15px');
    }

    // Remove extra logo
    if((jQuery('.front-in-mobile .logo').is(':visible'))&&(jQuery('.header-inner .logo').length > 0 )){
        //jQuery('.front-in-mobile .logo').css('display','none');

    }


    //detect IE
    function detectIE() {
        var ua = window.navigator.userAgent;
        var msie = ua.indexOf('MSIE ');
        if (msie > 0) {
            // IE 10 or older => return version number
            return parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10);
        }
        var trident = ua.indexOf('Trident/');
        if (trident > 0) {
            // IE 11 => return version number
            var rv = ua.indexOf('rv:');
            return parseInt(ua.substring(rv + 3, ua.indexOf('.', rv)), 10);
        }
        var edge = ua.indexOf('Edge/');
        if (edge > 0) {
            // Edge (IE 12+) => return version number
            return parseInt(ua.substring(edge + 5, ua.indexOf('.', edge)), 10);
        }
        // other browser
        return false;
    }

    if ( $.browser.msie ) {
        $('body').addClass('ie');
    }

    // detect IE
    var IEversion = detectIE();

    if (IEversion == '13') {
        $('body').addClass('ie-13');
    } else if (IEversion == '11') {
        $('body').addClass('ie-11');
    } else if (IEversion == '10') {
        $('body').addClass('ie-10');
    } else if (IEversion == '9') {
        $('body').addClass('ie-9');
    }


    if(jQuery('.front-in-mobile').length > 0 ){
        jQuery('.home-header .header-inner .nav-cta').css('right','20px');
    }

    equalHeightBox ();

    jQuery(window).scroll(function() {
        var height = jQuery(window).scrollTop();
        if(height  > 100) {
            //closeNavigation ();
        }
    });

    //mobileOnly();
    //devicesAll();

    jQuery('.home-header .header-inner ul li').append('<div class=nav-border></div>');

    jQuery('.careers-main .location-holder').not('.careers-main .location-holder.location-hover, .careers-main .location-holder.no-opportunity').each(function(e){
        jQuery(this).bind( 'click touchstart',function(e){

            e.preventDefault();

            var regionSelect = jQuery(this).attr('data-region-id');
            jQuery('.careers-main .location-holder').parent().removeClass('selected');
            jQuery(this).parent().addClass('selected');
            jQuery('.featured-job').hide();
            jQuery('.featured-job'+regionSelect).fadeIn(function(){
                jQuery('html, body')
                    .stop()
                    .animate({
                        scrollTop: jQuery('.careers-main .featured-job'+regionSelect).offset().top-50
                    }, 500,function(){
                    });
            });

            /**
             * list box height fix
             */
            global.helpers.listBoxHeightFix(jQuery('.featured-job'+regionSelect));


        });
    });


    jQuery(".slide-show").cycle({
        speed: 2000,
        timeout: 3000,
        slides: '.slider-content',
        next: '#next',
        prev: '#prev',
        pager: '#gallery-chooser ul',
        pagerActiveClass: 'selected',
        pagerTemplate: '',
        fx: 'fade',
        log: false,
        pauseOnHover: true
    });

    jQuery(".fancyBox").fancybox({
        maxWidth	: 1000,
        maxHeight	: 670,
        fitToView	: false,
        width		: '90%',
        height		: '80%',
        autoSize	: false,
        closeClick	: false,
        openEffect	: 'none',
        closeEffect	: 'none',
        helpers: {
            overlay: {
                locked: false
            }
        },
        afterShow : function () {



            // update overlay htmls
            var clickedJobid = jQuery(this.element).attr('data');
            var jobName = jQuery(this.element).attr('jname');
            var jobLocation = jQuery(this.element).attr('jlocation');

            //console.log(jobName)
            //console.log(jobLocation)

            var elem = jQuery('.fancybox-outer');

            $.each(jobList,function (k, v) {
                if(clickedJobid == v.id){


                    jobID = v.id;
                    slug = v.term_slug;
                    var fullSlug = '';
                    if(slug == 'sl'){
                        fullSlug = 'Sri Lanka';
                    }else if( slug == 'uk' ){
                        fullSlug = 'United Kingdom';
                    }

                    elem.find('.infoText').html('I would like to apply for the role of '+jobName+' based in '+jobLocation+'');
                    elem.find('.infoTexSlug').html(fullSlug);



                    elem.find('.jobid').each(function () {
                        jQuery(this).val("")
                            .val( v.id )
                    });
                    elem.find('.c_slug').each(function () {
                        jQuery(this).val("")
                            .val( v.term_slug )
                    });
                    elem.find('.icon').each(function () {
                        jQuery(this).removeAttr('class')
                            .addClass( v.dep_slug )
                            .addClass( 'icon' )
                    });
                    elem.find('.titleOverlay').each(function () {
                        jQuery(this).html( v.title )
                    });
                    elem.find('.job-detail-copy').each(function () {
                        jQuery(this).html( v.description )
                    });

                    elem.find('.fb').each(function () {
                        jQuery(this).attr("href","http://www.facebook.com/sharer.php?u="+ v.share_url)
                    });
                    elem.find('.twitter').each(function () {
                        jQuery(this).attr("href","https://twitter.com/share?url="+ v.share_url)
                    });
                    elem.find('.linkedin').each(function () {
                        jQuery(this).attr("href","http://www.linkedin.com/shareArticle?mini=true&url="+ v.share_url)
                    });
                    elem.find('.mail').each(function () {
                        jQuery(this).attr("href","mailto:jobs@volumepeople.com?subject="+ v.title)
                    });
                    elem.find('.forward-mail').each(function () {
                        jQuery(this).attr("href","mailto:?subject="+v.title+" Vacancy at Volume&body="+'Read more and apply at https://www.volumepeople.com/job/')
                    });
                }
            });


            //jQuery('.click-close').on( "click", function() {
            //    jQuery('.fancybox-opened').hide();
            //    jQuery('.fancybox-overlay').attr('style','display:none;');
            //
            //});
            setPopupHeight();
            jQuery(".nano").nanoScroller({
                alwaysVisible: true,
                iOSNativeScrolling: true,
            });
            jQuery('body').css({'overflow':'hidden'});
            jQuery('.fancybox-opened .uploadCV .message-block').removeClass('validation');
            jQuery('.fancybox-opened .uploadCV .message-block').removeClass('success');
            jQuery('.fancybox-opened .uploadCV .message-block').removeClass('progress');
            jQuery('.fancybox-opened .uploadCV .message-block label').text('');
        },
        afterClose : function () {
            jQuery('body').css({'overflow':'auto'});
                jobID = false;
                slug = false;
        },
        onUpdate: function(){
            setPopupHeight ();
           // jQuery(".nano").nanoScroller({
           //     alwaysVisible: true
            //});
        }
    });

    //waypoint handler for peoples page
     function wayInit() {
            jQuery('.people-main .full-width').each(function (k, v) {
                //console.log(k)
                waypointObj[k] = new Waypoint({
                    element: jQuery(this),
                    offset: '90px',
                    handler: function (direction) {
                        //if (jQuery('.home-header.nav-active').length < 1) {
                        if (direction == 'down') {
                            //nav.find('a[href="#' + this.element[0].id + '"]').addClass('active');
                            if (jQuery('.home-header .header-inner.nav-active').length < 1) {
                                var hotSpot = (this.element[0].id);
                                if (hotSpot != 1) {
                                    jQuery('.home-header').addClass('nav-dark');
                                } else {
                                    jQuery('.home-header').removeClass('nav-dark');
                                }
                            }
                        }
                        else {
                            console.log(this.element[0].id);
                            if (jQuery('.home-header .header-inner.nav-active').length < 1) {
                                var hotSpot = (this.element[0].id);
                                if (hotSpot == 2) {
                                    jQuery('.home-header').removeClass('nav-dark');
                                }
                            }
                        }
                        //}
                    }
                    //offset: jQuery('.nav-holder').outerHeight(true)
                })
            });
    }

    //waypoint handler for benefits page

    function wayInitBenefit() {
        jQuery('.benefits-main .full-width').each(function (k, v) {
            //console.log(k)
            waypointObjBenefit[k] = new Waypoint({
                element: jQuery(this),
                offset: '90px',
                handler: function (direction) {
                    //if (jQuery('.home-header.nav-active').length < 1) {
                    if (direction == 'down') {
                        //nav.find('a[href="#' + this.element[0].id + '"]').addClass('active');
                        if (jQuery('.home-header .header-inner.nav-active').length < 1) {
                            var hotSpot = (this.element[0].id);
                            console.log(hotSpot);
                            if (hotSpot != 1) {
                                jQuery('.home-header').addClass('nav-dark');
                            } else {
                                jQuery('.home-header').removeClass('nav-dark');
                            }
                        }
                    }
                    else {
                        console.log(this.element[0].id);
                        if (jQuery('.home-header .header-inner.nav-active').length < 1) {
                            var hotSpot = (this.element[0].id);
                            if (hotSpot == 2) {
                                jQuery('.home-header').removeClass('nav-dark');
                            }
                        }
                    }
                    //}
                }
                //offset: jQuery('.nav-holder').outerHeight(true)
            })
        });
    }

    //waypoint handler for careers page

    jQuery(window).load(function () {
        if (jQuery('.careers-main').length > 0){
            jQuery('.home-header').removeClass('nav-dark');
        }
        extraLogo();
       // jQuery(".nano").nanoScroller({
        //    alwaysVisible: true
        //});

        /**
         * @ToDo: Mokshi to review later - Thilina
         */
        if(((wWidth) < 740 )&& !(jQuery('body').hasClass('device-landscape'))&& !(jQuery('body').hasClass('iphone'))){
            //jQuery('body').addClass("phone-landscape");
        }
        else {
            //jQuery('body').removeClass("phone-landscape");
        }

        if (/Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent)) {
            if((wWidth) < 666 ) {
                jQuery('.job-overlay .bottom-content.share li').attr('style', 'margin-right: 10px !important');
            }
            else if(wWidth > 666 && wWidth < 801) {
                jQuery('.job-overlay .bottom-content.share li').attr('style', 'margin-right: 0 !important');
            }
        }

        if(!(/Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent)) && (wWidth) < 666 ) {
            jQuery('.fancybox-close').attr('style','right: 15px');
            jQuery('.job-overlay .job-detail .job-detail-copy').attr('style','padding-left: 10px');
        }
        else {
            //jQuery('.fancybox-close').attr('style','right: 15px');
        }
        //mobileOnly();
        //devicesAll();
        if( jQuery('body').hasClass("phone-landscape") && (jQuery(window).width() < 740) || jQuery('body').hasClass("device-portrait")) {
            jQuery('.job-list-cta.mobile').attr('style', 'display: none;');
            jQuery('.phone-landscape.body-jobs-page .vacant-show #pager').attr('style', 'display: none !important;')
        }

        if(jQuery(window).width() < 665){
            // var imgHeight = jQuery('.front-in-mobile div img').height();
            var imgHeight = (jQuery(window).height())/4;
            jQuery('.front-in-mobile div, .front-in-mobile div a').css('height',imgHeight)
            //alert(jQuery(window).height());
        }

    });

    //function addClose(){
    //   if( jQuery('.job-overlay .jobdetail-head').length > 0){
    //       jQuery('.job-overlay .jobdetail-head').append( "<a class='click-close'></a>" )
    //   }
    //
    //}

    if (/iPhone/i.test(navigator.userAgent)) {
        if (wWidth == 736) {
            jQuery('body').addClass("sixpluslandscape");
        }
    }
    if (/iPhone/i.test(navigator.userAgent)) {
        if (wWidth == 414) {
            jQuery('body').addClass("sixplusportrait");
        }
    }

    //function devicesAll(){
    //    if (/Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent)) {
    //        jQuery('.fancybox-close').attr('style', 'right: 15px !important;');
    //        //jQuery('.front-in-mobile .logo').attr('style',' display: block !important');
    //    }
    //}
    function mobileOnly(){

        var isMobile = {
            Android: function() { return navigator.userAgent.match(/Android/i); },
            BlackBerry: function() { return navigator.userAgent.match(/BlackBerry/i); },
            Opera: function() { return navigator.userAgent.match(/Opera Mini/i); },
            Windows: function() { return navigator.userAgent.match(/IEMobile/i); },
            any: function() { return (isMobile.Android() || isMobile.BlackBerry() || isMobile.Opera() || isMobile.Windows()); } };
        if (isMobile.any() && (wWidth < 768)){
            jQuery('.fancybox-wrap').addClass("is-mobile");
            jQuery('.fancybox-skin').attr('style','width: 100% !important; height: 100% !important;');
            jQuery('.fancybox-wrap').attr('style','width: 100% !important; height: 100% !important;');
            jQuery('.fancybox-wrap').attr('style',' left: 0 !important;top: 0 !important; position: fixed !important;');
            jQuery('.fancybox-inner').attr('style','width: 100% !important;overflow: visible !important;');
            jQuery('.fancybox-close').attr('style','right: 15px;');
            jQuery('.fancybox-skin').attr('style','overflow: auto;');
            jQuery('.bottom-content.uploadCV').attr('style',' width: 100% !important');

        }





    }

    function wayInitCareers() {
        jQuery('.careers-main .full-width').each(function (k, v) {
            //console.log(k)
            waypointObjCareers[k] = new Waypoint({
                element: jQuery(this),
                offset: '90px',
                handler: function (direction) {
                    //if (jQuery('.home-header.nav-active').length < 1) {
                    if (direction == 'down') {
                        //nav.find('a[href="#' + this.element[0].id + '"]').addClass('active');
                        if (jQuery('.home-header .header-inner.nav-active').length < 1) {
                            var hotSpot = (this.element[0].id);

                            if (hotSpot != 1) {
                                jQuery('.home-header').addClass('nav-dark');
                            } else {
                                jQuery('.home-header').removeClass('nav-dark');
                            }
                        }
                    }
                    else {
                        //console.log(this.element[0].id);
                        if (jQuery('.home-header .header-inner.nav-active').length < 1) {
                            var hotSpot = (this.element[0].id);
                            if (hotSpot == 2) {
                                jQuery('.home-header').removeClass('nav-dark');
                            }
                        }
                    }
                    //}
                }
                //offset: jQuery('.nav-holder').outerHeight(true)
            })
        });
    }

    function wayInitFooter() {
        jQuery('.parallax-holder .waypoint-wrapper').each(function (k, v) {
            //console.log(k)
            waypointObjFooter[k] = new Waypoint({
                element: jQuery(this),
                offset: 'bottom-in-view',
                handler: function (direction) {
                    if (direction == 'down') {
                        if (jQuery('.home-header .header-inner.nav-active').length < 1) {
                            openNavigation ();
                        }
                    }
                    else {
                        if (jQuery('.home-header .header-inner.nav-active').length > 0) {
                            closeNavigation ();
                        }
                    }
                }
            })
        });
    }

    function wayInitNavigation() {
        jQuery('.parallax-holder').each(function (k, v) {
            //console.log(k)
            waypointObjNavigation[k] = new Waypoint({
                element: jQuery(this),
                offset: '90px',
                handler: function (direction) {
                    //if (jQuery('.home-header.nav-active').length < 1) {
                    if (direction == 'down') {
                        //nav.find('a[href="#' + this.element[0].id + '"]').addClass('active');
                        //if (jQuery('.home-header .header-inner.nav-active').length < 1) {
                            //var hotSpot = (this.element[0].id);

                            /*if (hotSpot != 1) {
                                jQuery('.home-header').addClass('nav-dark');
                            } else {
                                jQuery('.home-header').removeClass('nav-dark');
                            }*/
                            closeNavigation ();
                        //}
                    }
                    else {
                        //console.log(this.element[0].id);
                        /*if (jQuery('.home-header .header-inner.nav-active').length < 1) {
                            var hotSpot = (this.element[0].id);
                            if (hotSpot == 2) {
                                jQuery('.home-header').removeClass('nav-dark');
                            }
                        }*/
                    }
                    //}
                }
                //offset: jQuery('.nav-holder').outerHeight(true)
            })
        });
    }

    //hide menu when click outside
    jQuery(document).on('mousedown', function (e) {
        if (jQuery(e.target).closest(".home-header").length === 0) {
            //closeNavigation ();
        }
    });

    //careers page animations
    jQuery('.careers-main .location-holder').not('.careers-main .location-holder.selected').each(function(){
      jQuery(this).hover(function(){
        //jQuery(this).toggleClass('selected');
      });
    });

    function extraLogo(){
        if(jQuery('.front-in-mobile .logo').length > 0) {
            //jQuery('.header-inner .wrap .logo').attr('style','display: none;');
        }
    }

    /**
     * @ToDo: Mokshi to review later - Thilina
     */
    jQuery(window).resize(function() {
        extraLogo();
        var wWidth = jQuery(window).width();
        //jQuery('body').removeClass("phone-landscape");
        if((wWidth) < 740 ) {
                //jQuery('body').addClass("phone-landscape");
        }
        else if((wWidth) > 740 ) {
            //jQuery('body').removeClass("phone-landscape");
        }
        else {
            //jQuery('body').removeClass("phone-landscape");
        }

        if(jQuery(window).width() < 665){
            // var imgHeight = jQuery('.front-in-mobile div img').height();
            var imgHeight = (jQuery(window).height())/4;
            jQuery('.front-in-mobile div, .front-in-mobile div a').css('height',imgHeight)
            //alert(jQuery(window).height());
        }


        jQuery(window).load(function(){
            clearTimeout(window.resizedFinished);
            window.resizedFinished = setTimeout(function(){
                console.log('Resized finished.');
               // jQuery(".nano").nanoScroller({
               //     alwaysVisible: true
               // });
                setPopupHeight ();
            }, 250);
            equalHeightBox ();
        });

    });

    function setPopupHeight () {
        var boxHeight = jQuery('.fancybox-opened .fancybox-inner').height();
        jQuery('.fancybox-opened .fancybox-inner').css({'overflow':'hidden'});
        //jQuery('body').css({'overflow':'hidden'});
        //jQuery('.fancybox-opened .fancybox-inner .nano-content').height(1200);
        jQuery('.fancybox-opened .fancybox-inner .detail-wrap.nano').css({'height':boxHeight-( global.helpers.fancyBoxSpace )});
        //jQuery(".nano").nanoScroller({
        //    alwaysVisible: true
        //});
    }

    function openNavigation (){
        jQuery('.home-header ul').slideDown();
        jQuery('.home-header .header-inner').addClass('nav-active open');
    }

    function closeNavigation (){
        jQuery('.home-header ul').slideUp();
        jQuery('.home-header .header-inner').removeClass('nav-active open');
    }

    jQuery(window).load(function(){
        if(jQuery('.home-header.landing-page').length < 1) {
            openNavigation();
        }
        //addClose();
        //devicesAll();
    });
    awardSetting();
    function awardSetting(){
        jQuery('.awards-logo li:first').addClass('selected');
        jQuery('.award-detail-wrap .award-detail:first').show();

        jQuery('.awards-logo li').each(function(){
            jQuery(this).click(function(e){
                e.preventDefault();
                jQuery('.awards-logo li').removeClass('selected');
                jQuery('.award-detail-wrap .award-detail').hide();
                var detailID = jQuery(this).find('a').attr('id');
                jQuery(this).addClass('selected')
                jQuery('.award-detail-wrap .award-detail.'+detailID).fadeIn();
            });
        });
    }
    equalHeightBox ();
    function equalHeightBox () {
        if(/Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent)){
            jQuery('.careers-main .job-list .list-holder li').addClass("mobile");
        }
        if(!(/Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent))){
            jQuery('.careers-main .job-list .list-holder li').addClass("desktop");
        }

        if( jQuery('.careers-main .job-list .list-holder li').hasClass("desktop") && (jQuery(window).width() < 1270)){
            jQuery('.job-list-cta').attr('style', 'display: none');
            jQuery('.job-summary').append('<div class="spacer" style="height: 15px; display: block"></div>');
            jQuery('.careers-main .job-list .list-holder .job-list-item .job-detail-sect .job-summary').attr('style', 'width: 90%');
            jQuery('.careers-main .job-list .list-holder li').attr('style', 'position: relative !important;');
            jQuery('.job-list-cta.mobile').attr('style', 'display: block; position: absolute !important; bottom: 20px; right: 20px; ');
        }

        if( jQuery('.careers-main .job-list .list-holder li').hasClass("desktop") && (jQuery(window).width() < 1065)){
            jQuery('.job-summary').append('<div class="spacer-smaller" style="height: 5px; display: block"></div>');
        }

        if( jQuery('.careers-main .job-list .list-holder li').hasClass("desktop") && (jQuery(window).width() < 800)){
            jQuery('.job-list-cta.mobile').attr('style', 'display: none')
            jQuery('.phone-landscape.body-jobs-page .vacant-show #pager').attr('style', 'display: none !important');
        }

        if( jQuery('.careers-main .job-list .list-holder li').hasClass("mobile")){
            jQuery('.job-list-cta').attr('style', 'display: none');

            jQuery('.job-summary').each(function(){
                if(jQuery(this).find('.spacer').length){
                    jQuery(this).find('.spacer').remove();
                }
                jQuery(this).append('<div class="spacer" style="height: 15px; display: block"></div>');
            })


            jQuery('.careers-main .job-list .list-holder .job-list-item .job-detail-sect .job-summary').attr('style', 'width: 95% !important');
            jQuery('.careers-main .job-list .list-holder li').attr('style', 'position: relative !important;');
            jQuery('.job-list-cta.mobile').attr('style', 'display: block; position: absolute !important; bottom: 20px; right: 20px; ');

            //set the starting bigestHeight variable


        }

        if((jQuery(window).width() < 480)){
            jQuery('.job-list-cta.mobile').attr('style', 'display: none!important; ');
        }
        jQuery('.careers-main .job-list .list-holder .job-list-item').css({'height':'auto'});
        var heightArray = jQuery('.careers-main .job-list .list-holder .job-list-item').map(function() {
            return  jQuery(this).height();
        }).get();
        var maxHeight = Math.max.apply(Math, heightArray);
        jQuery('.careers-main .job-list .list-holder .job-list-item').height(maxHeight);




    }
    parallaxSettings();
    function parallaxSettings () {
        //jQuery('.parallax-holder').addClass('active-parallax');
        jQuery('.parallax-holder').animate(
            {top:'25rem'}
            , 1500,'linear' ,function() {
            //callback
             wayInit();
             wayInitBenefit();
             wayInitCareers();
             wayInitFooter();
             wayInitNavigation();
             jQuery('.header-sub .copy-area').fadeIn();
        });

    }

    //check devices
    function CheckMobileDevice ()
    {
        var             deviceAgent         = navigator.userAgent.toLowerCase();
        var             isTouchDevice       = Modernizr.touch || (deviceAgent.match(/(iphone|ipod|ipad)/) || deviceAgent.match(/(android)/) || deviceAgent.match(/(iemobile)/) || deviceAgent.match(/iphone/i) || deviceAgent.match(/ipad/i) || deviceAgent.match(/ipod/i) || deviceAgent.match(/blackberry/i) || deviceAgent.match(/bada/i));
        return isTouchDevice;
    }

    function  CheckTablet () {
        var             windowWidth             = jQuery(window).width();
        var             tabs                    = (windowWidth >= 700) && (windowWidth <= 1280);
        return tabs;
    }

    function CheckMobile () {
        //device detection
        var windowWidth             = jQuery(window).width();
        var mobile                  = windowWidth < 700;

        if (CheckMobileDevice()) {
            if (mobile) {
                //jQuery('body').addClass('mobile-phone');
                return true;
            }
        } else {
            //do nothing
            return false;
        }
    }

    SetResize ();
    function SetResize () {
        var windowWidth = jQuery(window).width();
        var mobile = windowWidth < 700;

        /*if (CheckMobileDevice()) {
            if (CheckTablet()) {
                //do nothing
                jQuery(window).resize(function () {
                    var height = jQuery(window).height();
                    var width = jQuery(window).width();

                    if (width > height) {
                        // Landscape
                        jQuery('body').removeClass('set-overlay');
                        alert('landscape');
                         if(!navigator.userAgent.toLowerCase().match(/ipad/i))
                        {
                            //do nothing
                        }
                    } else {
                        jQuery('body').removeClass('set-overlay');
                        jQuery('body').addClass('set-overlay');
                        alert('portrait');
                    }
                });
            }
        } else*/
        if (CheckMobileDevice()) {
            if (mobile) {
                jQuery(window).resize(function () {
                    var height = jQuery(window).height();
                    var width = jQuery(window).width();
                    if (width > height) {
                        //Landscape
                        jQuery('body').removeClass('set-overlay');
                        //alert('landscape');
                    } else {
                        jQuery('body').removeClass('set-overlay');
                        jQuery('body').addClass('set-overlay');
                        //alert('portrait');
                    }
                });
            }

        }

    }
    function SetResize () {
        var windowWidth = jQuery(window).width();
        var mobile = windowWidth < 700;

        /*if (CheckMobileDevice()) {
            if (CheckTablet()) {
                //do nothing
                jQuery(window).resize(function () {
                    var height = jQuery(window).height();
                    var width = jQuery(window).width();

                    if (width > height) {
                        // Landscape
                        jQuery('body').removeClass('set-overlay');
                        alert('landscape');
                         if(!navigator.userAgent.toLowerCase().match(/ipad/i))
                        {
                            //do nothing
                        }
                    } else {
                        jQuery('body').removeClass('set-overlay');
                        jQuery('body').addClass('set-overlay');
                        alert('portrait');
                    }
                });
            }
        } else*/
        if (CheckMobileDevice()) {
            if (mobile) {
                jQuery(window).resize(function () {

                    var height = jQuery(window).height();
                    var width = jQuery(window).width();
                    if (width > height) {
                        //Landscape
                        jQuery('body').removeClass('set-overlay');
                    } else {
                        jQuery('body').removeClass('set-overlay');
                        jQuery('body').addClass('set-overlay');
                    }
                });
            }

        }

    }
    //jQuery(window).on("orientationchange",function(){
    //    jQuery('.fancybox-close').attr('style','right: 15px');
    //
    //    mobileOnly();
    //    //devicesAll();
    //
    //    equalHeightBox ();
    //});

    jQuery(window).resize(function () {

        mobileResponsive();
        var wWidth = jQuery(window).width();
        if(!(/Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent)) && (wWidth) < 666 ) {
           jQuery('.fancybox-close').attr('style','right: 15px');
            jQuery('.job-overlay .job-detail .job-detail-copy').attr('style','padding-left: 10px');
        }
        else {
           // jQuery('.fancybox-close').attr('style','right: 15px');
        }


        jQuery('.job-list-cta').attr('style', 'display: block; ');
        jQuery('.job-list-cta.mobile').attr('style', 'display: none; ');

        function removeSpacer(){
            jQuery('.spacer-smaller').remove();
            jQuery('spacer').remove();
            jQuery('spacer-big').remove();
        }
        removeSpacer();

        //jQuery('.careers-main .job-list .list-holder .job-list-item .job-detail-sect .job-summary').attr('style',
        // 'width: 75%');
        var contentHeight = jQuery('.careers-main .job-list .list-holder .job-list-item .job-detail-sect').height();

        if( jQuery('.careers-main .job-list .list-holder li').hasClass("desktop") && (jQuery(window).width() < 1290)){
            removeSpacer();
            jQuery('.job-list-cta').attr('style', 'display: none; ');
            jQuery('.careers-main .job-list .list-holder li').attr('style', 'position: relative');
            jQuery('.job-list-cta.mobile').attr('style', 'display: block; position: absolute !important; bottom: 20px; right: 20px; ');
            jQuery('.careers-main .job-list .list-holder .job-list-item .job-detail-sect .job-summary').attr('style', 'width: 85% !important');

            var contentHeight = jQuery('.careers-main .job-list .list-holder .job-list-item .job-detail-sect').height();
            if(jQuery(contentHeight) < 100 ){
                alert("dfdf");
                jQuery('.careers-main .job-list .list-holder .job-list-item .job-detail-sect').css('height', contentHeight + "100px");
            }
        }
        if( jQuery('.careers-main .job-list .list-holder li').hasClass("desktop") && (jQuery(window).width() < 1085)){
            removeSpacer();
            jQuery('.careers-main .job-list .list-holder .job-list-item .job-detail-sect .job-summary').attr('style', 'width: 85%');
        }
        if( jQuery('.careers-main .job-list .list-holder li').hasClass("desktop") && (jQuery(window).width() < 740)){
            removeSpacer();
            //jQuery('.job-summary').append('<div class="spacer-big" style="height: 20px; display: block"></div>');
            jQuery('.job-list-cta.mobile').attr('style', 'display: block; position: absolute !important; bottom: 20px; right: 20px; ');

        }
        if( jQuery('body').hasClass("phone-landscape") && (jQuery(window).width() < 740) || jQuery('body').hasClass("device-portrait")) {
            jQuery('.job-list-cta.mobile').attr('style', 'display: none;');
            jQuery('.phone-landscape.body-jobs-page .vacant-show #pager').attr('style', 'display: none !important;')
        }
        jQuery('.careers-main .job-list .list-holder .job-list-item').css({'height':'auto'});
        var heightArray = jQuery('.careers-main .job-list .list-holder .job-list-item').map(function() {
            return  jQuery(this).height();
        }).get();
        var maxHeight = Math.max.apply(Math, heightArray);
        //jQuery('.careers-main .job-list .list-holder .job-list-item').height(maxHeight);


    });

    //detect iphone4/5 landscape only
    if( /iPhone/i.test(navigator.userAgent) ) {

        jQuery('body').addClass('iphone');
    }

    var mobile= {};
    var mobileResponsive = function() {
        var w = $(window).height()/4;
        if (CheckMobileDevice()) {
            var height = jQuery(window).height();
            var width = jQuery(window).width();
            mobile = width < 740;
            if (mobile) {
                if (width > height) {
                    //Landscape
                    jQuery('body').removeClass('device-portrait').addClass('device-landscape');
                    $('#fsvs-body .slide.nth-class-1').removeClass('active-slide').detach().appendTo('#front-page');
                    $('#fsvs-body .slide.nth-class-2').addClass('active-slide');

                    $('#fsvs-body .slide.nth-class-2').find('.arrow.down').attr('id', '1');
                    $('#fsvs-body .slide.nth-class-3').find('.arrow.down').attr('id', '2');
                    $('#fsvs-body .slide.nth-class-4').find('.arrow.down').attr('id', '3');

                    $('#fsvs-body .slide.nth-class-3').find('.arrow.up').attr('id', '0');
                    $('#fsvs-body .slide.nth-class-4').find('.arrow.up').attr('id', '1');
                    $('#fsvs-body .slide.nth-class-5').find('.arrow.up').attr('id', '2');

                    //$('.front-in-mobile div, .front-in-mobile div a').css('height', '100%');
                } else {
                    jQuery('body').removeClass('device-landscape').addClass('device-portrait');
                    //jQuery('body').removeClass('phone-landscape');
                    //slider2.unbind();
                    $('.front-in-mobile div, .front-in-mobile div a').css('height', w);
                }
            }
            else {
                $('#front-page .slide.nth-class-1').addClass('active-slide').detach().prependTo('#fsvs-body');
                $('#fsvs-body .slide.nth-class-2').removeClass('active-slide');
            }
        }

    }

    mobileResponsive();

    var mobileNavbar = function() {
        if ($("body").hasClass("device-portrait")) {
            $(window).scroll(function(e){
                if ($(this).scrollTop() > 50){
                $('.header-inner').addClass('nav-active-mob open');
            }
                else{
                    $('.header-inner').removeClass('nav-active-mob open');
                }
            });

            jQuery(".vacant-show").cycle({
                speed: 100,
                timeout: 0,
                slides: '.vacant-content',
                next: '#next',
                prev: '#prev',
                pager: '#pager',
                pagerActiveClass: 'pager',
                pagerTemplate: '',
                fx: 'slide',
                log: false,
                pauseOnHover: true,
                //swipe:true
            });



            parallaxMob();
            function parallaxMob () {
                jQuery('.parallax-holder').animate(
                    {top:'15rem'}
                    , 500,'linear' ,function() {
                        //callback
                        wayInit();
                        wayInitBenefit();
                        wayInitCareers();
                        wayInitFooter();
                        wayInitNavigation();
                        jQuery('.header-sub .copy-area').fadeIn();
                    });
            }


            $('.page-button').click(function(){
                if($('#Wokingham').hasClass('cycle-slide-active')){
                    var slideHeight = $('#Wokingham').height() + 20;
                    $('.vacant-show').height(slideHeight);
                }

                if($('#Plymouth').hasClass('cycle-slide-active')){
                    var slideHeight = $('#Plymouth').height() + 20;
                    $('.vacant-show').height(slideHeight);
                }

                if($('#Colombo').hasClass('cycle-slide-active')){
                    var slideHeight = $('#Colombo').height() + 20;
                    $('.vacant-show').height(slideHeight);
                }

            });

        }
    }
    mobileNavbar();


    //The below script is a hack for iphone new version tab bar on top.
    (function(doc) {

        var addEvent = 'addEventListener',
            type = 'gesturestart',
            qsa = 'querySelectorAll',
            scales = [1, 1],
            meta = qsa in doc ? doc[qsa]('meta[name=viewport]') : [];

        function fix() {
            meta.content = 'width=device-width,minimum-scale=' + scales[0] + ',maximum-scale=' + scales[1];
            doc.removeEventListener(type, fix, true);
        }

        if ((meta = meta[meta.length - 1]) && addEvent in doc) {
            fix();
            scales = [.25, 1.6];
            doc[addEvent](type, fix, true);
        }

    }(document));

    // End Script


});

