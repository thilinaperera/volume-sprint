// create minified versions of the .css
// https://github.com/gruntjs/grunt-contrib-cssmin
module.exports = function( grunt ){
    return {
        deploy: {
            expand: true,
            cwd: '<%= options.deploy %>/styles',
            src: ['*.css', '!*-min.css'],
            dest: '<%= options.deploy %>/styles',
            ext: '-min.css'
        }
    };
};
