// Watch files for changes

module.exports = function(grunt) {
	return {
        libs : {
            src : ['js/lib/modernizr-2.8.3.js','js/lib/matchMedia.js','js/lib/matchMedia.addListener.js','js/lib/enquire.min.js','js/lib/dmuploader.min.js','js/lib/jquery.form.min.js','js/lib/jquery.validate.min.js','js/lib/bundle.js','js/lib/html5-placeholder.js','js/lib/jquery.nanoscroller.min.js','js/lib/jquery.waypoints.min.js','js/lib/jquery.cycle2.min.js','js/lib/jquery.cycle2.swipe.min.js','js/lib/jquery.fancybox.js'],
            dest : 'js/build/libs.js'
        },
		dist: {
            src: [
                'js/dev/*.js', //All JS in the libs folder
            ],
            dest: 'js/build/production.js',
        }
	};
};