// Minify production.js

module.exports = function(grunt) {
	return {
        libs: {
            src: 'js/build/libs.js',
            dest: 'js/build/libs.min.js'
        },
        deploy: {
            src: 'js/build/production.js',
            dest: 'js/build/production.min.js'
        }
	};
};