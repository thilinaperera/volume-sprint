// make a zipfile
module.exports = function( grunt ){
    return {
        deploy: {
                options: {
                archive: '<%= options.deploy %>zips/rdt-typography.zip'
              },
              files: [
                {expand: true,cwd: '<%= options.deploy %>', src: ['styles/rdt-typo*.*','fonts/hps/**'], dest: '', filter: 'isFile'}
              ]
            }
      }
    }    