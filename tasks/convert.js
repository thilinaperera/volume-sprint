// converts one file format to another eg. XML to JSON
// https://github.com/assemble/grunt-convert
module.exports = function( grunt ){
    return {
         options: {
             explicitArray: false,
          },
          json2xml: {
              options: {
                  xml: {
                    header: true
                  }
                },
            src: ['src/sites/rds/docs/ui-architecture.json'],
              dest: 'src/sites/rds/docs/ui-architecture2.xml'
          },

           xml2json: {
             files: [
               {
                expand: true,
                cwd: '<%= options.src %>', 
                src: ['**/*.xml'],
                dest: '<%= options.src %>',
                ext: '.json'
              }
            ]
           },
            xml2yml: {
              src: ['src/sites/rds/docs/ui-architecture2.xml'],
              dest: 'src/sites/rds/docs/ui-architecture2.yml'
            }
    };
};