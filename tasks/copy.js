// Copy the files to the build/deploy directories.

module.exports = function(grunt) {
	return {
        build: {
        	files: [
                {
                    expand: true,
                    src: [
                        'fonts/**'
                    ],
                    dest: '<%= options.build %>',
                    filter: 'isFile'
                },
        		{
        			expand: true,
        			src: [
        				'images/**',
                        'docs/**'
        			],
        			dest: '<%= options.build %>',
                    filter: 'isFile'
        		},
                {
                    expand: true,
                    src: [
                        'styles/**'
                    ],
                    dest: '<%= options.build %>',
                    filter: 'isFile'
                },
                {
                    expand: true,
                    src: [
                        'js/build/*',
                        'js/lib/*'
                    ],
                    dest: '<%= options.build %>'
                },
                {
                    expand: true,
                    src: [
                        '*.mp4',
                        '*.webm',
                        '*.pdf',
                        '*.php',
                        '*.css',
                        '*.map',
                        '*.txt',
                        'screenshot.*',
                        'includes/**',
                        'acf/**'//,
                        //'immersive/**'
                        //'!includes/build-head.php'
                    ],
                    dest: '<%= options.build %>'
                }
        	]    
        },
        local: {
        	files: [
                {
                    expand: true,
                    src: [
                        'plugins/**'
                    ],
                    dest: '<%= options.local %>',
                    filter: 'isFile'
                },
                {
                    expand: true,
                    src: [
                        'fonts/**'
                    ],
                    dest: '<%= options.local %>',
                    filter: 'isFile'
                },
        		{
        			expand: true,
        			src: [
        				'images/**',
                        'docs/**'

                    ],
        			dest: '<%= options.local %>',
                    filter: 'isFile'
        		},
                {
                    expand: true,
                    src: [
                        'styles/**'
                    ],
                    dest: '<%= options.local %>',
                    filter: 'isFile'
                },
                {
                    expand: true,
                    src: [
                        'js/build/*',
                        'js/lib/*'
                    ],
                    dest: '<%= options.local %>'
                },
                {
                    expand: true,
                    src: [
                        '*.mp4',
                        '*.webm',
                        '*.php',
                        '*.css',
                        '*.map',
                        '*.txt',
                        'screenshot.*',
                        'includes/**',
                        'acf/**'//,
                        //'immersive/**'
                        //'!includes/build-head.php'
                    ],
                    dest: '<%= options.local %>'
                }
        	]
        },
        deploy: {
        	files: [
                {
                    expand: true,
                    src: [
                        'fonts/**'
                    ],
                    dest: '<%= options.deploy %>',
                    filter: 'isFile'
                },
                {
                    expand: true,
                    src: [
                        'images/**',
                        'docs/**'

                    ],
                    dest: '<%= options.deploy %>',
                    filter: 'isFile'
                },
                {
                    expand: true,
                    src: [
                        'styles/**'
                    ],
                    dest: '<%= options.deploy %>',
                    filter: 'isFile'
                },
                {
                    expand: true,
                    src: [
                        'js/deploy/*',
                        'js/lib/*',
                        'js/build/*'
                    ],
                    dest: '<%= options.deploy %>'
                },
                {
                    expand: true,
                    src: [
                        '*.mp4',
                        '*.webm',
                        '*.php',
                        '*.css',
                        '*.map',
                        '*.txt',
                        'screenshot.*',
                        'includes/**',
                        'immersive/**',
                        'acf/**'
                    ],
                    dest: '<%= options.deploy %>'
                }
            ]
        },
        css:{
            files: [
                {
                    expand: true,
                    src: [
                        '*.css'
                    ],
                    dest: '<%= options.local %>'
                }
            ]
        }
    };
};