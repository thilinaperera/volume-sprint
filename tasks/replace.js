// Replace call to non-minified js in deploy

module.exports = function(grunt) {
	return {
        deploy: {
			src: ['<%= options.deploy %>/**/*.php'],
			overwrite: true,
			replacements: [
				{
					from: '?v=VERSION',
					to: '?v=<%= pkg.cache %>'
				},
			    {
				   	from: 'build/production.js', 
				   	to: 'deploy/production.min.js'
				},
				{
					from: '/hp/ENVIRONMENT/',
					to: function () {
						if (grunt.option('itg') == true || grunt.option('ITG') == true){
							return '/hp/merlin_ng_dev/';
						} else if (grunt.option('development') == true){
							return '/hp/merlin_ng_dev/';
						} else if (grunt.option('production') == true){
							return '/hp/merlin_ng_pro/';
						} else {
							return '/hp/deploy-test/';
						}
					}
				},
                {
                    from: 'BINGENVIRONMENT',
                    to: function () {
                        if (grunt.option('itg') == true || grunt.option('ITG') == true){
                            return 'SproutStoresITG';
                        } else if (grunt.option('development') == true){
                            return 'SproutStoresDev';
                        } else if (grunt.option('production') == true){
                            return 'SproutStores';
                        } else {
                            return 'SproutStoresDev';
                        }
                    }
                },
                {
                    from: 'SERVERENVIRONMENT',
                    to: function () {
                        if (grunt.option('itg') == true || grunt.option('ITG') == true){
                            return 'itg';
                        } else if (grunt.option('development') == true){
                            return 'development';
                        } else if (grunt.option('production') == true){
                            return 'production';
                        } else {
                            return 'development';
                        }
                    }
                },
                {
                    from: 'itg.storage.sprout.hp.com/liquid',
                    to: function () {
                        if (grunt.option('production') == true){
                            return 'storage.sprout.hp.com/liquid';
                        } else {
                            return 'itg.storage.sprout.hp.com/liquid';
                        }
                    }
                },
                {
                    from: "ID_US",
                    to: function () {
                        if (grunt.option('itg') == true || grunt.option('ITG') == true){
                            return '"1d1050c41358429c85c649d2d44bb236"';
                        } else if (grunt.option('development') == true){
                            return '"81ca31c8a0364cba97fd81d2542b38dc"';
                        } else if (grunt.option('production') == true){
                            return '"2fc4fef02a16425abc0bde6e196f2d00"';
                        } else {
                            return '"81ca31c8a0364cba97fd81d2542b38dc"';
                        }
                    }
                },
                {
                    from: "ID_UK",
                    to: function () {
                        if (grunt.option('itg') == true || grunt.option('ITG') == true){
                            return '"2fa352d7863a4c31ab0bcb50b7d2f89e"';
                        } else if (grunt.option('development') == true){
                            return '"8e9c1b0eade74fb9987c40eb69256104"';
                        } else if (grunt.option('production') == true){
                            return '"8638c5640d8a4192bc8e597def80e396"';
                        } else {
                            return '"8e9c1b0eade74fb9987c40eb69256104"';
                        }
                    }
                },

                {
                    from: "ID_FR",
                    to: function () {
                        if (grunt.option('itg') == true || grunt.option('ITG') == true){
                            return '"2437f1402d354e219652633a9561488d"';
                        } else if (grunt.option('development') == true){
                            return '"7f08a9c07ac74fd4bc236239878c2a54"';
                        } else if (grunt.option('production') == true){
                            return '"22cfb09325af455897c43a0d02d10502"';
                        } else {
                            return '"7f08a9c07ac74fd4bc236239878c2a54"';
                        }
                    }
                },

                {
                    from: "ID_ES",
                    to: function () {
                        if (grunt.option('itg') == true || grunt.option('ITG') == true){
                            return '"179cdcedb6a84940b1d18eb7ac601b03"';
                        } else if (grunt.option('development') == true){
                            return '"fe460b857dd749fc90a1f7be9bfb6afd"';
                        } else if (grunt.option('production') == true){
                            return '"40a27ca061404ad3bc2761be104f9424"';
                        } else {
                            return '"fe460b857dd749fc90a1f7be9bfb6afd"';
                        }
                    }
                },


                {
                    from: 'QIDTHING',
                    to: function () {
                        if (grunt.option('itg') == true || grunt.option('ITG') == true){
                            return '22978';
                        } else if (grunt.option('development') == true){
                            return '22978';
                        } else if (grunt.option('production') == true){
                            return '23388';
                        } else {
                            return '22978';
                        }
                    }
                },
                {
                    from: 'OWNCHECKYES',
                    to: function () {
                        if (grunt.option('itg') == true || grunt.option('ITG') == true){
                            return '698823';
                        } else if (grunt.option('development') == true){
                            return '698823';
                        } else if (grunt.option('production') == true){
                            return '714546';
                        } else {
                            return '698823';
                        }
                    }
                },
                {
                    from: 'OWNCHECKNO',
                    to: function () {
                        if (grunt.option('itg') == true || grunt.option('ITG') == true){
                            return '698824';
                        } else if (grunt.option('development') == true){
                            return '698824';
                        } else if (grunt.option('production') == true){
                            return '714547';
                        } else {
                            return '698824';
                        }
                    }
                },
                {
                    from: 'OWNOPTIONID',
                    to: function () {
                        if (grunt.option('itg') == true || grunt.option('ITG') == true){
                            return 'aid_s_c_274638';
                        } else if (grunt.option('development') == true){
                            return 'aid_s_c_274638';
                        } else if (grunt.option('production') == true){
                            return 'aid_s_c_278127';
                        } else {
                            return 'aid_s_c_274638';
                        }
                    }
                },
			]
		},
        local: {
		    src: ['<%= options.local %>/**/*.php'],
		    overwrite: true,                 
		    replacements: [
		    	{
					from: '?v=VERSION',
					to: '?v=<%= pkg.cache %>'
				},
				{
					from: '/hp/ENVIRONMENT/',
					to: '/hp/merlin_dev/'
				},
                {
                    from: 'BINGENVIRONMENT',
                    to: 'SproutStoresDev'
                },
                {
                    from: 'SERVERENVIRONMENT',
                    to: 'development'
                },
                {
                    from: "ID_UK",
                    to: '"8e9c1b0eade74fb9987c40eb69256104"'
                },
                {
                    from: "ID_US",
                    to: '"81ca31c8a0364cba97fd81d2542b38dc"'
                },
                {
                    from: "ID_FR",
                    to: '"7f08a9c07ac74fd4bc236239878c2a54"'
                },
                {
                    from: "ID_ES",
                    to: '"fe460b857dd749fc90a1f7be9bfb6afd"'
                },
                {
                    from: 'QIDTHING',
                    to: '22978'
                },
                {
                    from: 'OWNCHECKYES',
                    to: '698823'
                },
                {
                    from: 'OWNCHECKNO',
                    to: '698824'
                },
                {
                    from: 'OWNOPTIONID',
                    to: 'aid_s_c_274638'
                },
                {
                    from: '"proxy"   => "tcp://proxy.austin.hp.com:8080",',
                    to: ''
                }
			]
		},
        build: {
		    src: ['<%= options.build %>/**/*.php'],
		    overwrite: true,
		    replacements: [
		    	{
					from: '?v=VERSION',
					to: '?v=<%= pkg.cache %>'
				},
				{
					from: '/hp/ENVIRONMENT/',
					to: '/hp/merlin_dev/'
				},
                {
                    from: 'BINGENVIRONMENT',
                    to: 'SproutStoresDev'
                },
                {
                    from: 'SERVERENVIRONMENT',
                    to: 'development'
                },
                {
                    from: "ID_UK",
                    to: '"8e9c1b0eade74fb9987c40eb69256104"'
                },
                {
                    from: "ID_US",
                    to: '"81ca31c8a0364cba97fd81d2542b38dc"'
                },
                {
                    from: "ID_FR",
                    to: '"7f08a9c07ac74fd4bc236239878c2a54"'
                },
                {
                    from: "ID_ES",
                    to: '"fe460b857dd749fc90a1f7be9bfb6afd"'
                },
                {
                    from: 'QIDTHING',
                    to: '22978'
                },
                {
                    from: 'OWNCHECKYES',
                    to: '698823'
                },
                {
                    from: 'OWNCHECKNO',
                    to: '698824'
                },
                {
                    from: 'OWNOPTIONID',
                    to: 'aid_s_c_274638'
                },
                {
                    from: '"proxy"   => "tcp://proxy.austin.hp.com:8080",',
                    to: ''
                }
			]
		}
	};
};
