<?php
/**
 * Global Variables
 */

define('THEMEROOT', get_stylesheet_directory_uri());
define('IMAGES', THEMEROOT . '/images');
define('JAVASCRIPTS', THEMEROOT . '/js');
define('STYLES', THEMEROOT . '/styles');
define('VERSION', '1.6.0');


include_once(get_stylesheet_directory() . '/includes/build-plugins.php');


add_action('after_setup_theme', 'sprint_setup');
function sprint_setup()
{
    add_action('wp_enqueue_scripts', 'calling_dashicons');   // wp_enqueue_scripts is the action , calling_dashicons is the function
    add_action('init', 'flush_rewrite_rules');
}

/**
 * Dashboard Icons
 */
function calling_dashicons()
{
    wp_enqueue_style('dashicons'); // dashicons is a core icon set up
}


/**
 * Extra - Add Volume logo & url
 */
function my_login_logo()
{
    ?>
    <style type="text/css">
        body.login div#login h1 a {
            background-image: url(<?php echo get_bloginfo( 'template_directory' ) ?>/images/large-logo.png);
            display: block;
            background-size: contain;
        }
    </style>
    <?php
}

add_action('login_enqueue_scripts', 'my_login_logo');
function my_login_logo_url()
{
    return get_bloginfo('url');
}

add_filter('login_headerurl', 'my_login_logo_url');

/**
 * Extra - Add favicon
 */

function add_favicon()
{
    $favicon_url = get_stylesheet_directory_uri() . '/images/favicon.ico';
    echo '<link rel="shortcut icon" href="' . $favicon_url . '" />';
}

add_action('login_head', 'add_favicon');
add_action('admin_head', 'add_favicon');

/**
 * Extra - Remove wordpress logo
 */
function annointed_admin_bar_remove()
{
    global $wp_admin_bar;

    /* Remove their stuff */
    $wp_admin_bar->remove_menu('wp-logo');
}

add_action('wp_before_admin_bar_render', 'annointed_admin_bar_remove', 0);

/**
 * Extra - Remove admin bar links
 */
function remove_admin_bar_links()
{
    global $wp_admin_bar;
    $wp_admin_bar->remove_menu('feedback');         // Remove the feedback link
    $wp_admin_bar->remove_menu('updates');          // Remove the updates link
    $wp_admin_bar->remove_menu('comments');         // Remove the comments link
    $wp_admin_bar->remove_menu('new-content');      // Remove the content link
    $wp_admin_bar->remove_menu('w3tc');             // If you use w3 total cache remove the performance link
}

add_action('wp_before_admin_bar_render', 'remove_admin_bar_links');

/**
 * customize ACF path
 */

add_filter('acf/settings/path', 'my_acf_settings_path');
function my_acf_settings_path($path)
{

    // update path
    $path = get_stylesheet_directory() . '/acf/';

    // return
    return $path;

}

/**
 *  customize ACF dir
 */
add_filter('acf/settings/dir', 'my_acf_settings_dir');

function my_acf_settings_dir($dir)
{

    // update path
    $dir = get_stylesheet_directory_uri() . '/acf/';

    // return
    return $dir;

}

/**
 * Hide ACF field group menu item
 */
add_filter('acf/settings/show_admin', '__return_false');


/**
 * Include ACF
 */
include_once(get_stylesheet_directory() . '/acf/acf.php');

// add unique id field
add_action('acf/include_field_types', 'include_field_types_unique_id');
function include_field_types_unique_id($version)
{
    include_once(get_stylesheet_directory() . '/acf/acf-unique_id-v5.php');
}

/**
 *  Include custom fields
 */
include 'includes/build-fields.php';


/**
 * acf_custom_css
 */

add_action('admin_head', 'acf_custom_css');

function acf_custom_css()
{
    echo '<style>
    .acf-th-bing_id {
        width: 50px;
    }
    .acf-th-bing_status {
        width: 50px;
    }
    .th-event_zip_or_postcode {
        width: 90px;
    }
  </style>';
}


/**
 * remove the wysiwyg editor
 */
function reset_editor()
{
    global $_wp_post_type_features;

    $post_type = "page";
    $feature = "editor";
    if (!isset($_wp_post_type_features[$post_type])) {

    } elseif (isset($_wp_post_type_features[$post_type][$feature]))
        unset($_wp_post_type_features[$post_type][$feature]);
}

add_action("init", "reset_editor");


/**
 * Register Header Menu
 */

function register_sprint_header_menu()
{
    register_nav_menu('header-menu', __('Header'));

}

add_action('init', 'register_sprint_header_menu');


/**
 * ODD/EVEN Checking
 * @param  integer $num Number of the posts
 * @return string Output will be EV/OD according to the number of the posts
 */
function oeChecker($num)
{

    if ($num % 2 == 1) {
        return "EV";
    } else {
        return "OD";
    }
}

/**
 * helpers - remove special chars
 * @param string $string Passing the class name here
 * @return string Cleaned class name
 */
function clean($string)
{
    $string = str_replace(' ', '-', $string);
    $string = preg_replace('/[^A-Za-z0-9\-]/', '', $string);
    return strtolower(preg_replace('/-+/', '-', $string));
}



function scripts_styles_method() {


    wp_enqueue_style(
        'style',THEMEROOT.'/style.css', array(  ), VERSION
    );
    wp_enqueue_style(
        'uploader',STYLES.'/uploader.css', array(  ), VERSION
    );


    wp_enqueue_script(
        'libs',JAVASCRIPTS.'/build/libs.min.js', array( 'jquery' ), VERSION
    );
    wp_enqueue_script(
        'production',JAVASCRIPTS.'/build//production.js', array( 'jquery' ), VERSION
    );

    wp_localize_script(
        'production', // this needs to match the name of our enqueued script
        'ajaxPath',      // the name of the object
        array(
            'ajaxurl' => get_stylesheet_directory_uri().'/ajax-uploader.php',
            'themeroot' => get_stylesheet_directory_uri(),
        ) // the property/value
    );




}
add_action( 'wp_enqueue_scripts', 'scripts_styles_method' );


/**
 *  Disable plugin auto update
 */

add_filter( 'auto_update_plugin', '__return_false' );

/**
 * Turn off all error reporting
 */
error_reporting(0);

/**
 *Custom rss feed
 */


add_action( 'after_setup_theme', 'my_rss_template' );
/**
 * Register custom RSS template.
 */
function my_rss_template() {
    //add_feed( 'short', 'my_custom_rss_render' );
    remove_all_actions( 'do_feed_rss2' );
    add_action( 'do_feed_rss2', 'my_custom_rss_render', 10, 1 );
}

/**
 * Custom RSS template callback.
 */
function my_custom_rss_render() {
    get_template_part( 'rss', 'feed' );
}


add_filter( 'pre_get_posts', 'wpdb_add_cpt_feed' );
function wpdb_add_cpt_feed( $query ) {
    if ( $query->is_feed() )
        $query->set( 'post_type', array( 'post', 'jobman_job' ) );
    return $query;
}


/**
 * fix for cookie error while login.
 */

setcookie(TEST_COOKIE, 'WP Cookie check', 0, COOKIEPATH, COOKIE_DOMAIN);
if ( SITECOOKIEPATH != COOKIEPATH ){
    setcookie(TEST_COOKIE, 'WP Cookie check', 0, SITECOOKIEPATH, COOKIE_DOMAIN);
}
